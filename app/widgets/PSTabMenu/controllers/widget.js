/*
 *  ---------
 *  PSTabMenu
 *  ---------
 *  @version - 1.0
 *  @author  - Thet Paing Soe
 *  @company - Panacea-Soft
 *  Copyright (c) 2014, 2015 Panacea-Soft All Right Reserved
 */

var args = arguments[0] || {};

// private variables
var 
	_scrWidth = Ti.Platform.displayCaps.platformWidth,
	_tmpMovedPoints = 0,
	_currentPage = 0,
	_tabWidth = 130,
	_tabHeight = 40,
	_tabPadding = 15,
	_currentPoints = 0,
	_movedPoints = _currentPoints,
	_font = null,
	_focusColor = 'red',
	_color = 'black',
	_backgroundColor = 'white',
	_totalTabCount = 0,
	_tabList = [],
	_viewList = [];
	
if(Ti.Platform.osname != "android") {
	var _aniSpeed1 = 250,
	 	_aniSpeed2 = 200,
	 	_aniSpeed3 = 160;
}else{
	var _aniSpeed1 = 100,
		_aniSpeed2 = 80,
		_aniSpeed3 = 50;
}

	
	
// private functions	
var _resetAllTabColor = function(){
	for(var i=0; i<_tabList.length; i++) {
		_tabList[i].color = _color;
	}
};

var _updateTabStyle = function(index) {
	_resetAllTabColor();
	
	if(_tabList.length>0) { 
		_tabList[index].color = _focusColor;
	} 
};

var _moveTo = function(index){
	
	if(_totalTabCount > index) {
		
		$.viewContainer.scrollToView(index);
		
		_currentPage = index;
		_updateTabStyle(index);
		_moveLeft(index);
	}
};

var _setBackgroundColor = function(color){
	_backgroundColor = color;
	
	$.container.backgroundColor = _backgroundColor;
};

var _setFocusColor = function(color) {
	_focusColor = color;
	
	_updateTabStyle(_currentPage);
};

var _setColor = function(color) {
	_color = color;
	
	_updateTabStyle(_currentPage);
};
var _getScreenDpi = function() {
	//if (dpi == 0 | dpi == null) {
	var	dpi = Ti.Platform.displayCaps.dpi;
	//}
	return dpi;
};
var _convertPxtoDp = function(px) {
	return px / (_getScreenDpi() / 160);
};

var _moveLeft = function(currentPage) {
	//Ti.API.info("MOVED POINTS : "+_movedPoints+" LEFT POINTS "+_currentPoints+" Screen Width "+ _scrWidth + " tab width "+ _tabWidth + " Padding "+ _tabPadding);
	//Ti.API.info("total Padding " + (_tabPadding*((2*(currentPage)))) + "half width "+(_tabWidth/2));
	if(Ti.Platform.osname != "android") {
		_currentPoints -= ((_tabWidth+ (_tabPadding*2) )); //( (_tabWidth+(_tabPadding*2)) - _movedPoints);
	}else{
		_currentPoints -=_tabWidth;	
	}
	
	//Ti.API.info(" Current Page : "+currentPage+" Menu Left : "+$.tabContainer.left+" _current Pints "+_currentPoints+" Total Should : "+((currentPage+1) * _tabWidth) +" Actual : "+($.tabContainer.left + _currentPoints));
	
	// actual points
	var _tmpCurPage = currentPage;
	var _forTabPoints = (_scrWidth / 2 )-((((_tabWidth+ (_tabPadding*2) )) * currentPage) + ((_tabWidth/2) + _tabPadding));
	
	//Ti.API.info("SHOULD : "+_forTabPoints);
	_currentPoints = _forTabPoints;
	
	
	//Ti.API.info(" Current Page : "+currentPage+" Menu Left : "+$.tabContainer.left+" _current Pints "+_currentPoints+" Should : " + _forTabPoints);
	
	$.tabContainer.animate({
		left : _currentPoints,
		duration : _aniSpeed1//100//250
	}, function(){
		$.tabContainer.animate({
			left : _currentPoints+4,//17,
			duration : _aniSpeed2//80//200
		}, function(){
			$.tabContainer.animate({
				left : _currentPoints,
				duration : _aniSpeed3//50//160
			});
			_movedPoints = 0;
			var params = {
				tab : _currentPage
			};
			Ti.App.fireEvent('PSTabChangeEvent', params);
		});
	});
};

var _moveRight = function(currentPage) {
	
	if(Ti.Platform.osname != "android") {
		_currentPoints += ((_tabWidth+ (_tabPadding*2) )); //( (_tabWidth+(_tabPadding*2)) - _movedPoints);
	}else{
		_currentPoints +=_tabWidth;	
	}
	
	var _tmpCurPage = currentPage;
	var _forTabPoints = (_scrWidth / 2 )-((((_tabWidth+ (_tabPadding*2) )) * currentPage) + ((_tabWidth/2) + _tabPadding));
	
	//Ti.API.info("SHOULD : "+_forTabPoints);
	_currentPoints = _forTabPoints;
	
	
	
	$.tabContainer.animate({
		left : _currentPoints,
		duration : _aniSpeed1//100//250
	}, function(){
		$.tabContainer.animate({
			left : _currentPoints-4,//17,
			duration : _aniSpeed2//80//200
		}, function(){
			$.tabContainer.animate({
				left : _currentPoints,
				duration : _aniSpeed3// 50//160
			});
			_movedPoints = 0;
			var params = {
				tab : _currentPage
			};
			Ti.App.fireEvent('PSTabChangeEvent', params);
		});
	});
	
};

var _UIAdjust = function(currentPage) {
	//Ti.API.info("MOVED POINTS : "+_movedPoints+" LEFT POINTS "+_currentPoints+" Screen Width "+ _scrWidth + " tab width "+ _tabWidth + " Padding "+ _tabPadding);
	//Ti.API.info("total Padding " + (_tabPadding*((2*(currentPage)))) + "half width "+(_tabWidth/2));
	if(Ti.Platform.osname != "android") {
		_currentPoints -= ((_tabWidth+ (_tabPadding*2) )); //( (_tabWidth+(_tabPadding*2)) - _movedPoints);
	}else{
		_currentPoints -=_tabWidth;	
	}
	
	//Ti.API.info(" Current Page : "+currentPage+" Menu Left : "+$.tabContainer.left+" _current Pints "+_currentPoints+" Total Should : "+((currentPage+1) * _tabWidth) +" Actual : "+($.tabContainer.left + _currentPoints));
	
	// actual points
	var _tmpCurPage = currentPage;
	var _forTabPoints = (_scrWidth / 2 )-((((_tabWidth+ (_tabPadding*2) )) * currentPage) + ((_tabWidth/2) + _tabPadding));
	
	//Ti.API.info("SHOULD : "+_forTabPoints);
	_currentPoints = _forTabPoints;
	
	
	//Ti.API.info(" Current Page : "+currentPage+" Menu Left : "+$.tabContainer.left+" _current Pints "+_currentPoints+" Should : " + _forTabPoints);
	
	$.tabContainer.animate({
		left : _currentPoints,
		duration : _aniSpeed1//100//250
	}, function(){
		$.tabContainer.animate({
			left : _currentPoints+4,//17,
			duration : _aniSpeed2//80//200
		}, function(){
			$.tabContainer.animate({
				left : _currentPoints,
				duration : _aniSpeed3//50//160
			});
			_movedPoints = 0;
			
		});
	});
};


if(Ti.Platform.osname != "android") {
	$.viewContainer.addEventListener('scroll', function(e){
		try{
		if(e.currentPageAsFloat >= 0) {
			
			var _tmpCurPageAsFloat = _currentPage - e.currentPageAsFloat;
			
			//Ti.API.info(" TMP CUR : "+_tmpCurPageAsFloat+" AS FLOAT : "+ e.currentPageAsFloat + " PAGE : "+ e.currentPage+ " e : "+JSON.stringify(e));
			
			//check where user move
			if(_tmpMovedPoints!=0) {
				
				if(_tmpMovedPoints > e.currentPageAsFloat) {
					
					_currentPoints -= _tmpCurPageAsFloat; 
					//update the current Points
					_movedPoints -= _tmpCurPageAsFloat;
					
				 }else{
					
					_currentPoints += _tmpCurPageAsFloat;
					
					//update the current Points
					_movedPoints += _tmpCurPageAsFloat;
					
				}
				
			}
			
			_tmpMovedPoints = _tmpCurPageAsFloat;
			
			$.tabContainer.left  = _currentPoints;//JSON.stringify(e) );
			
			if(_tmpCurPageAsFloat == 0) {
				
				_UIAdjust(e.currentPage);
				_currentPage = e.currentPage;
				
				
			}
		}
		}catch(e){}
	});
}

$.viewContainer.addEventListener('scrollEnd', function(){
	
	var currentPage = $.viewContainer.getCurrentPage();
	
	if(_currentPage != currentPage){
		
		if(_currentPage < currentPage) {
			_moveLeft(currentPage);
		}else{
			_moveRight(currentPage);
		}
		
		_currentPage = currentPage;
		
		_updateTabStyle(_currentPage);
	}
});
// end private functions


// public functions
exports.init = function (args) {
	
	if(Ti.Platform.osname == "android") {
		_scrWidth = _convertPxtoDp(_scrWidth);
	}
		
	// get tab Width 
	if(args.tabWidth) {
		_tabWidth = args.tabWidth;
		
		_currentPoints = (_scrWidth/2) - (_tabWidth/2);
		_movedPoints = _currentPoints;
	}else{
		_tabWidth = 100;
		
		_currentPoints = (_scrWidth/2) - (_tabWidth/2);
		_movedPoints = _currentPoints;
	}
	
	$.highlight.left = (_scrWidth /2 ) - (_tabWidth / 2);
	//Ti.API.info("LEFT : "+$.highlight.left);
	// get tab height
	if(args.tabHeight) {
		_tabHeight = args.tabHeight;
		$.tabContainer.height = _tabHeight+'dp';	
	}else{
		_tabHeight = 40;
		$.tabContainer.height = _tabHeight+'dp';
	}
	
	// get tab Padding
	if(args.tabPadding) {
		_tabPadding = args.tabPadding;
		_currentPoints -= _tabPadding;
		_movedPoints = _currentPoints;
	}
	
	if(args.tabStyle){
		if(args.tabStyle.font){
			_font = args.tabStyle.font;
		}
		
		if(args.tabStyle.focusColor) {
			_focusColor = args.tabStyle.focusColor;
		}
		
		if(args.tabStyle.color) {
			_color = args.tabStyle.color;
		}
		
		if(args.tabStyle.backgroundColor) {
			_setBackgroundColor(args.tabStyle.backgroundColor);
		}
		
		if(args.tabStyle.barColor){
			$.highlight.backgroundColor = args.tabStyle.barColor;
		}
	}
	
	if(args.tabViews) {
		
		_totalTabCount = args.tabViews.length;
		for(var i=0; i<args.tabViews.length; i++) {
			var tab = Ti.UI.createLabel({
				left : _tabPadding + "dp",
				width : _tabWidth + "dp",
				right : _tabPadding + "dp",
				height : _tabHeight,
				textAlign : "center",
				text : args.tabViews[i].view.tabName,
				tabIndex : i
			});
			
			if(_font){
				tab.font = _font;
			}
			
			if(_color) {
				tab.color = _color;
			}
			
			tab.addEventListener('click', function(e){
				_moveTo(e.source.tabIndex);
			});
					
			_tabList[i] = tab;
			$.tabContainer.add(tab);
			
			_viewList[i] = args.tabViews[i].view.tabView;
			
			$.viewContainer.addView(args.tabViews[i].view.tabView);
			
		}
	}
	//Ti.API.info("CURRENT POINTS"+_currentPoints);
	$.tabContainer.left = _currentPoints;
	
	_updateTabStyle(0);
	
};

exports.getCurrentTab = function(){
	return _currentPage;
};

exports.moveTo = function(index){
	
	if(_totalTabCount > index) {
		$.viewContainer.scrollToView(index);
		
		_currentPage = index;
		_updateTabStyle(index);
		_moveLeft(index);
	}
};

exports.setBackgroundColor = function(color){
	_setBackgroundColor(color);
};

exports.setFocusColor = function(color) {
	_setFocusColor(color);
};

exports.setColor = function(color) {
	_setColor(color);
};

exports.addView = function(args) {
	if(args.view) {
		
		_totalTabCount += 1;
		//for(var i=0; i<args.tabViews.length; i++) {
		var tab = Ti.UI.createLabel({
			left : _tabPadding + "dp",
			width : _tabWidth + "dp",
			right : _tabPadding + "dp",
			height : _tabHeight,
			textAlign : "center",
			text : args.view.tabName,
			tabIndex : _totalTabCount-1
		});
		
		if(_font){
			tab.font = _font;
		}
		
		if(_color) {
			tab.color = _color;
		}
		
		tab.addEventListener('click', function(e){
			_moveTo(e.source.tabIndex);
		});
				
		_tabList[_tabList.length] = tab;
		$.tabContainer.add(tab);
		
		_viewList[_viewList.length] = args.view.tabView;
		
		$.viewContainer.addView(args.view.tabView);
			
		//}
	}
	
	_updateTabStyle(0);
};
