// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
Alloy.Collections.users  = Alloy.createCollection('users');
Alloy.Collections.basket = Alloy.createCollection('basket');
Alloy.Collections.item_attribute = Alloy.createCollection('item_attribute');
Alloy.Globals.data = null;
Alloy.Globals.loadSingleShop            = function(){};
Alloy.Globals.shakeSubCategoryTitle     = function(){};
Alloy.Globals.loadSelectedSubCategories = function(){};
Alloy.Globals.refreshBasket             = function(){};
Alloy.Globals.itemCountRefresh          = function(){};
Alloy.Globals.closeBasket               = function(){}; 
Alloy.Globals.closeCheckout             = function(){};
Alloy.Globals.closeItemDetail           = function(){};
Alloy.Globals.closeItemList             = function(){};
Alloy.Globals.closePaymentOption        = function(){};
Alloy.Globals.itemCountRefreshItemList  = function(){};
Alloy.Globals.itemCountRefreshIndex     = function(){};
Alloy.Globals.showSelectedCategoryName  = function(){};
