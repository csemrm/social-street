var args = arguments[0] || {};
var psAnimation = require('animation');
//var __ = require("platformSupport");
//var fontIconLoader = require("icomoonlib");
//var loadingWindow = require('loadingWindow');

var gotoProductList = function(e) {
	/*
	args.selectedMainCatName = $.subCategory.mainCatName;
	args.selectedSubCategoryIndex = $.subCategory.serialId;
	console.log("Serial For Tab : >>>>>>>>>>>>>>>> " + args.selectedSubCategoryIndex);
	var contentView = Alloy.createController("itemList", args).getView();
	psAnimation.in(contentView);
	*/
	
	args.selectedMainCatName = args.categoryName;
	args.selectedSubCategoryIndex = args.categoryId;
	console.log("Serial For Tab : >>>>>>>>>>>>>>>> " + args.selectedSubCategoryIndex);
	var contentView = Alloy.createController("itemList", args).getView();
	psAnimation.in(contentView);
};


var init = function() {
	var tmpWidth = 150;
	if(Alloy.isTablet){
		tmpWidth = 185;
	}
	
	$.subCategory.width = tmpWidth;
	var width = tmpWidth * 0.80;
	
	$.imSubCatImage.width = width;
	$.imSubCatImage.height = (width/250 * 200);
	
	
	//$.subCategory.serialId = args.serialId;
	//$.subCategory.subCatId = args.subCatId;
	//$.subCategory.mainCatId = args.mainCatId;
	//$.subCategory.mainCatName = args.mainCatName;
	$.imSubCatImage.image = args.categoryImage;
	$.lblSubCatName.text = args.categoryName.toString();
	//console.log(args.subCatImage);
	//console.log(args.subCatName);
	applyAnimation();
	
	//loadingWindow.endLoading();
	
};

var applyAnimation = function() {
	psAnimation.slowlyAppear($.subCategory);
};

init();
