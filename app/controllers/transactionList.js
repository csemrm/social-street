var args = arguments[0] || {};
var psAnimation = require('animation');
var __ = require('platformSupport');
var loader = require("loader");
var dialogBox = require("psdialog");
var fontIconLoader = require("icomoonlib");
var loadingWindow = require('loadingWindow');

var init = function() {
	loadUserTransactions();
};

var loadUserTransactions = function() {
	
	if(Titanium.Network.online == true) {
		var loaderArgs = {
			callbackFunction : callBackLoadUserTransactions,
			url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getUserTransactions + Ti.App.Properties.getString("userId")
		};
		loader.get(loaderArgs);
	} else {
		dialogBox.loadCustomDialog("Transaction", Alloy.CFG.Languages.offlineMessage);
	}
};

var callBackLoadUserTransactions = function(respData) {
	if(respData != null) {
		
		if(respData.error){
			$.dataNotFoundView.opacity = 1;
			$.dataNotFoundView.height = Ti.UI.SIZE;
			var notFoundIcon = fontIconLoader.getIcon("panacea","megaphone",35,{color:Alloy.CFG.Colors.MainColor});
			$.imgNotFound.image = notFoundIcon;
			$.lblNotFoundMessage.text = "Oops! Data Not Found From Server.";
			loadingWindow.endLoading();
		} else {
			
			$.transListView.removeAllChildren();
			for(var i=0; i < respData.length; i++)
			{
				oneTrans = Alloy.createController('transactionRowLayout', respData[i]).getView();
				$.transListView.add(oneTrans);		
			}
		}
		
	} else {
		console.log("Something was wrong at transaction api.");
	}
};

init();
