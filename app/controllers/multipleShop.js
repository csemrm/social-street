var __ = require('platformSupport');
var loadingWindow = require('loadingWindow');
var myAnimation = require('animation');
var loader = require("loader");
var strings = require("strings");
var args = arguments[0] || {};

var FIXED_ITEM_BIG_WIDTH = 213;
var FIXED_ITEM_BIG_HEIGHT = 150;
var FIXED_ITEM_SMALL_WIDTH = 107;
var FIXED_ITEM_SMALL_HEIGHT = 50;
var FIXED_SCREEN_WIDTH = 320; 
var ITEM_BIG_HEIGHT = 0;
var ITEM_BIG_WIDTH = 0;
var ITEM_SMALL_HEIGHT = 0;
var ITEM_SMALL_WIDTH = 0;
var SCREEN_WIDTH = 0;
var SCREEN_HEIGHT = 0;

var loadShopLeftLayout = function(shopData) {
	var params = {
		ITEM_BIG_WIDTH    : ITEM_BIG_WIDTH,
		ITEM_BIG_HEIGHT   : ITEM_BIG_HEIGHT,
		ITEM_SMALL_WIDTH  : ITEM_SMALL_WIDTH,
		ITEM_SMALL_HEIGHT : ITEM_SMALL_HEIGHT,
		shopData          : shopData
	};
	var layout = Alloy.createController('shopLeftLayout', params).getView();
	return layout;
};

var loadShopRightLayout = function(shopData) {
	var params = {
		ITEM_BIG_WIDTH    : ITEM_BIG_WIDTH,
		ITEM_BIG_HEIGHT   : ITEM_BIG_HEIGHT,
		ITEM_SMALL_WIDTH  : ITEM_SMALL_WIDTH,
		ITEM_SMALL_HEIGHT : ITEM_SMALL_HEIGHT,
		shopData          : shopData
	};
	var layout = Alloy.createController('shopRightLayout', params).getView();
	return layout;
};


var calculateSize = function() {
	SCREEN_WIDTH =  __.getScreenWidth();
	SCREEN_HEIGHT = __.getScreenHeight();
	ITEM_BIG_WIDTH = ((SCREEN_WIDTH / FIXED_SCREEN_WIDTH) * FIXED_ITEM_BIG_WIDTH) - 1;
	ITEM_BIG_HEIGHT = ((ITEM_BIG_WIDTH / FIXED_ITEM_BIG_WIDTH ) * FIXED_ITEM_BIG_HEIGHT)-1;
	ITEM_SMALL_WIDTH = ((SCREEN_WIDTH / FIXED_SCREEN_WIDTH) * FIXED_ITEM_SMALL_WIDTH)-1;
	ITEM_SMALL_HEIGHT = ((ITEM_SMALL_WIDTH / FIXED_ITEM_SMALL_WIDTH) * FIXED_ITEM_SMALL_HEIGHT)-1;
};

var init = function() {
	if(args != null) {
		calculateSize();
	
		$.shopScrollView.contentWidth = SCREEN_WIDTH;
		$.shopScrollView.width = SCREEN_WIDTH;
		$.shopScrollView.height = Ti.UI.FILL;
		
		for(var i=0;i<args.shops.length; i++){
			if(i==0 | (i%2==0)) {
				$.shopScrollView.add(loadShopLeftLayout(args.shops[i]));
			} else {
				$.shopScrollView.add(loadShopRightLayout(args.shops[i]));
			}
		}
	}
	loadingWindow.endLoading();
};

init();