var args = arguments[0] || {};
var fontIconLoader = require("icomoonlib");
var psAnimation = require('animation');
var __ = require('platformSupport');
var loadingWindow = require('loadingWindow');

if (__.isiOS7Plus()) {
	$.AppWrapper.top = 20;
}

var init = function() {
	loadingWindow.endLoading();
	loadIcon();
	
	console.log("Success Status :> " + args.success_status);
	
	if(args.success_status == 1) {
		$.lblSuccessMessage.text = Alloy.CFG.Languages.lblSuccessMessage;
	} else if(args.success_status == 2) {
		$.lblSuccessMessage.text = Alloy.CFG.Languages.lblSuccessMessageNoShopEmail;
	} else if(args.success_status == 3) {
		$.lblSuccessMessage.text = Alloy.CFG.Languages.lblSuccessMessageNoUserEmail;
	} else if(args.success_status == 4) {
		$.lblSuccessMessage.text = Alloy.CFG.Languages.lblSuccessMessageNoBothEmail;
	}
	
	applyAnimation();
	$.mainTitle.text = Alloy.CFG.Languages.lblPaymentSuccessful;
};

var applyAnimation = function() {
	psAnimation.slowlyAppear($.paymentSuccessView);
};

var closeWindow = function() {
	psAnimation.out($.paymentSuccess);
};

var doOkay = function() {
	closeWindow();
};

var loadIcon = function() {
	var backIcon = fontIconLoader.getIcon("panacea","back",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgBack.image = backIcon;
	
	var successIcon = fontIconLoader.getIcon("panacea","paperplane",35,{color:Alloy.CFG.Colors.MainColor});
	$.imgSuccess.image = successIcon;
};

init();
