var args = arguments[0] || {};
var strings = require('strings');
var __ = require('platformSupport');
var fontIconLoader = require("icomoonlib");
var loadingWindow = require('loadingWindow');
var psAnimation = require('animation');

var init = function() {
	
	$.lblItemName.text = Alloy.CFG.Languages.lblItemName + args.item_name;
	$.lblPrice.text = Alloy.CFG.Languages.lblPrice + args.unit_price;
	$.lblQty.text = Alloy.CFG.Languages.lblQty + args.qty;
	 
};

init();
