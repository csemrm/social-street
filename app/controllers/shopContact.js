var args = arguments[0] || {};
var __ = require('platformSupport');
var psAnimation = require('animation');
var fontIconLoader = require("icomoonlib");
var loader = require("loader");
var validation = require("validationRules");
var dialogBox = require("psdialog");
var loadingWindow = require('loadingWindow');
var changeFlag = 0;
var FIXED_PORTRAIT_WIDTH = 400;
var FIXED_PORTRAIT_MULTIPLY = 241;
var FIXED_LANDSCAPE_WIDTH = 600;
var FIXED_LANDSCAPE_MULTIPLY = 441;
var screenWidth = __.getScreenWidth();
var viewWidth = 0;

if (__.isiOS7Plus()) {
	$.AppWrapper.top = 20;
}


var closeWindow = function() {
	psAnimation.out($.shopContact);
};

var loadIcon = function() {
	var phoneIcon = fontIconLoader.getIcon("panacea","phone-square-pin",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgPhone.image = phoneIcon;
	
	var locationIcon = fontIconLoader.getIcon("panacea","address-square-pin",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgLocation.image = locationIcon;
	
	var emailIcon = fontIconLoader.getIcon("panacea","envelope",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgInquiryEmail.image = emailIcon;
	
	var nameIcon = fontIconLoader.getIcon("panacea","user",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgInquiryName.image = nameIcon;
	
	var messageIcon = fontIconLoader.getIcon("panacea","comment",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgInquiryMessage.image = messageIcon;
	
	var backIcon = fontIconLoader.getIcon("panacea","back",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgBack.image = backIcon;
	
};

var doContactProcess = function() {
	if(validationChecking()) {
		if(Titanium.Network.online == true) {
			
			loadingWindow.startLoading();
			
			var payloadJSON = {"name":$.txtName.value, "email": $.txtEmail.value, "message":$.txtContactMessage.value};
			
			var loaderArgs = {
				callbackFunction : callBackDoContactProcess,
				url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postContactUs + Ti.App.Properties.getString("selected_shop_id"),
				payload          : payloadJSON,
			};
			loader.post(loaderArgs);
			
		} else {
			dialogBox.loadCustomDialog("Contact Us", Alloy.CFG.Languages.offlineMessage);
		}
	}
};

var callBackDoContactProcess = function(respData) {
	
	loadingWindow.endLoading();
	
	if(respData.success) {
		$.txtName.value = "";
		$.txtEmail.value = "";
		$.txtContactMessage.value = "";
		dialogBox.loadCustomDialog("Contact Us", Alloy.CFG.Languages.inquirySuccessMessage);
	} else {
		dialogBox.loadCustomDialog("Contact Us", Alloy.CFG.Languages.somethingWrong);
	}
	
};

var doFollowShop = function() {
	if(Titanium.Network.online == true) {
		
		if(Ti.App.Properties.getString("userId")) {
			
			loadingWindow.startLoading();
			
			var payloadJSON = {"appuser_id":Ti.App.Properties.getString("userId")};
			
			var loaderArgs = {
				callbackFunction : callBackDoFollowShop,
				url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postFollowShop + Ti.App.Properties.getString("selected_shop_id"),
				payload          : payloadJSON,
			};
			loader.post(loaderArgs);
		
		} else {
			dialogBox.loadCustomDialog("Follow", Alloy.CFG.Languages.needForFollowMesssage);
		}
		
	} else {
		dialogBox.loadCustomDialog("Follow", Alloy.CFG.Languages.offlineMessage);
	}
};


var callBackDoFollowShop = function(respData) {
	loadingWindow.endLoading();
	if(respData != null) {
		if(respData.success) {
			$.lblFollowerCount.text = respData.total;
			$.btnFollowShop.opacity = 0;
			$.btnFollowShop.height = 0;
			Ti.App.Properties.setString("selected_shop_follow_count",respData.total);
			dialogBox.loadCustomDialog("Follow", Alloy.CFG.Languages.followSuccessMessage);
		} else {
			dialogBox.loadCustomDialog("Follow", Alloy.CFG.Languages.somethingWrong);
		}
	}
};

var validationChecking = function() {
	
	if($.txtEmail.value=="") {
		validation.validationFailAction($.txtEmail); 
    	changeFlag = 0;
    	return false;
	} else {
		
		if(!validation.validateEmail($.txtEmail.value)) {
			validation.validationFailAction($.txtEmail); 
    		changeFlag = 0;
    		return false;
		}
	}
	
	if($.txtName.value=="") {
		validation.validationFailAction($.txtName); 
    	changeFlag = 0;
    	return false;
	}
	
	if($.txtContactMessage.value=="") {
		validation.validationFailAction($.txtContactMessage); 
    	changeFlag = 0;
    	return false;
	}
	
	return true;
	
};

var isFollowedChecking = function() {
	
	if(Ti.App.Properties.getString("userId")) {
		var payloadJSON = {"appuser_id":Ti.App.Properties.getString("userId")};
		
		var loaderArgs = {
			callbackFunction : callBackIsFollowedChecking,
			url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postIsFollowed + Ti.App.Properties.getString("selected_shop_id"),
			payload          : payloadJSON,
		};
		loader.post(loaderArgs);
	}
	
};

var callBackIsFollowedChecking = function(respData) {
	if(respData != null) {
		if(respData.status == "yes") {
			$.btnFollowShop.opacity = 0;
			$.btnFollowShop.height = 0;
		}
	}
};

//alert(Alloy.CFG.Languages.lblItems);

var loadlanguage = function() {
	$.mainTitle.text = Alloy.CFG.Languages.lblShopInformation;
	
	$.lblInquiryMessage.text = Alloy.CFG.Languages.lblInquiryMessage;
	$.lblYourName.text = Alloy.CFG.Languages.lblYourName;
	$.lblEmail.text = Alloy.CFG.Languages.lblEmail;
	$.btnSubmit.text = Alloy.CFG.Languages.btnSubmit;
	$.btnFollowShop.title = Alloy.CFG.Languages.lblFollowShop;
	
	
	
	$.lblItems.text = Alloy.CFG.Languages.lblItems;
	$.lblCategories.text = Alloy.CFG.Languages.lblCategories;
	$.lblFollowers.text = Alloy.CFG.Languages.lblFollowers;
	$.txtContactMessage.hintText = Alloy.CFG.Languages.hintTextInquiryMessage;
	$.txtName.hintText = Alloy.CFG.Languages.hintTextYourName;
	$.txtEmail.hintText = Alloy.CFG.Languages.hintTextEmail;
	
	$.btnSubmit.title = Alloy.CFG.Languages.btnSendInquiry;
};

var init = function() {
	
	var padding = 20;
		
	var size = []; 
	size.width = Ti.App.Properties.getString("selected_shop_image_width");
	size.height = Ti.App.Properties.getString("selected_shop_image_height");
	
	var tmp = __.getGridPhotoSizeCalWidth(size,screenWidth);
	$.shopImagesView.width = tmp.width;
	$.shopImagesView.height = tmp.height;		

	$.shopImagesView.image = Ti.App.Properties.getString("selected_shop_image");
	$.lblImgDesc.text = Ti.App.Properties.getString("selected_shop_image_description");
	$.lblTitle.text = Ti.App.Properties.getString("selected_shop_name");
	$.lblDescription.text = Ti.App.Properties.getString("selected_shop_description");
	
	$.lblItemCount.text = Ti.App.Properties.getString("selected_shop_item_count");
	$.lblCategoryCount.text = Ti.App.Properties.getString("selected_shop_category_count");
	$.lblFollowerCount.text = Ti.App.Properties.getString("selected_shop_follow_count");
	
	//alert($.lblFollowerCount.text);
	
	
	if(Ti.App.Properties.getString("selected_shop_phone") == "") {
		$.phoneView.opacity = 0;
		$.phoneView.height = 0;
	} else {
		$.phoneView.opacity = 1;
		$.phoneView.height = Ti.UI.SIZE;
		$.lblPhoneNo.text = Ti.App.Properties.getString("selected_shop_phone").toString();
		
	}
	
	if(Ti.App.Properties.getString("selected_shop_address") == "") {
		$.locationView.opacity = 0;
		$.locationView.height = 0;
	} else {
		$.locationView.opacity = 1;
		$.locationView.height = Ti.UI.SIZE;
		$.lblLocation.text = Ti.App.Properties.getString("selected_shop_address");
	}

		
	$.midContainer.applyProperties({
		width : screenWidth,
		contentWidth : screenWidth
	});
	
	$.infoContainer.applyProperties({
		left : padding,
		right : padding
	});
	
	viewWidth = ((screenWidth - 10) / 3) - 1;
	
	$.countsView.width = screenWidth - 10;
	$.productCountView.width = viewWidth;
	$.categoryCountView.width = viewWidth;
	$.followerCountView.width = viewWidth;
	
	loadIcon();
	loadlanguage();
	isFollowedChecking();
	applyAnimation();
	
	if(!OS_IOS) {
		$.lblInquiryMessage.top = 15;
		$.lblYourName.top = 17;
		$.lblEmail.top = 5;
		
		$.txtContactMessage.blur();
		$.txtName.blur();
		$.txtEmail.blur();
		
		$.lblItemCount.color     = "#000";
		$.lblCategoryCount.color = "#000";
		$.lblFollowerCount.color = "#000";
		
		$.lblImgDesc.text = __.readMoreText(45,Ti.App.Properties.getString("selected_shop_image_description"));
	}

	
};

var applyAnimation = function() {
	psAnimation.slowlyAppear($.infoContainer);
	psAnimation.slowlyAppear($.shopImageView);
};

init();
