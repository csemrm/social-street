var args = arguments[0] || {};
var psAnimation = require('animation');
var __ = require('platformSupport');
var loader = require("loader");
var psScrollView = require('psScrollView');
var loadingWindow = require('loadingWindow');
var fontIconLoader = require("icomoonlib");
var selectedShopId;
var dialogBox = require("psdialog");
_psScrollView = new psScrollView();

if (__.isiOS7Plus()) {
    $.AppWrapper.top = 20;
}

var closeWindow = function() {
    psAnimation.out($.feedsList);
};

_psScrollView.addEventListener("InfiniteScrolling", function(e) {
    Ti.API.info("InfiniteScrolling event -> start loading data");
    var limit = Alloy.CFG.LimitOfPosts;
    var from = Ti.App.Properties.getInt("From");
    var loaderArgs = {
        callbackFunction : callBackInfiniteScrolling,
        url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getFeedsByShop + selectedShopId + "/count/" + limit + '/from/' + from
    };
    console.log(Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getFeedsByShop + selectedShopId + "/count/" + limit + '/from/' + from);
    loader.get(loaderArgs);
});

var updateFromVal = function() {
    var limit = Alloy.CFG.LimitOfPosts;
    var from = Ti.App.Properties.getInt("From");
    Ti.App.Properties.setInt("From", (limit + from));
};

var callBackInfiniteScrolling = function(respData) {
    if (respData != null) {
        for (var i = 0; i < respData.length; i++) {
            Ti.API.info('respData' + JSON.stringify(respData));
            oneFeed = Alloy.createController('feedsRow', respData[i]).getView();
            _psScrollView.addView(oneFeed);
        }
        updateFromVal();
        _psScrollView.infiniteScrollingCompleted();
    }
};

var init = function() {
    selectedShopId = args.shopData.id;
    loadFeeds(selectedShopId);
    var backIcon = fontIconLoader.getIcon("panacea", "back", 35, {
        color : Alloy.CFG.Colors.IconWhite
    });
    $.imgBack.image = backIcon;
};

var loadFeeds = function(selectedShopId) {
    if (Titanium.Network.online == true) {
        loadingWindow.startLoading();
        var limit = Alloy.CFG.LimitOfPosts;
        Ti.App.Properties.setInt("From", 0);
        var loaderArgs = {
            callbackFunction : callBackLoadFeeds,
            url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getFeedsByShop + selectedShopId + "/count/" + limit + '/from/0'
        };
        console.log(Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getFeedsByShop + selectedShopId + "/count/" + limit + '/from/0');
        loader.get(loaderArgs);
    }
};

var callBackLoadFeeds = function(respData) {
    loadingWindow.endLoading();
    if (respData != null) {
        _psScrollView.setViewLayout("vertical");
        $.feedListView.add(_psScrollView);

        if (respData.length > 0) {

            for (var i = 0; i < respData.length; i++) {
                oneFeed = Alloy.createController('feedsRow', respData[i]).getView();
                _psScrollView.addView(oneFeed);
            }

        } else {
            dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.noFeeds);
            closeWindow();
        }

        Ti.App.Properties.setInt("From", 5);
    }
};

init();
