var __ = require('platformSupport');
var psAnimation = require('animation');
var fontIconLoader = require("icomoonlib");
var loader = require("loader");
var loadingWindow = require('loadingWindow');
var dialogBox = require("psdialog");

var sideMenuIcon = fontIconLoader.getIcon("panacea","navicon",35,{color:Alloy.CFG.Colors.IconWhite});
var profileIcon = fontIconLoader.getIcon("panacea","user2",35,{color:Alloy.CFG.Colors.SideMenuIconColor});
var homeIcon = fontIconLoader.getIcon("panacea","home",35,{color:Alloy.CFG.Colors.SideMenuIconColor});
var switchShopIcon = fontIconLoader.getIcon("panacea","switch",35,{color:Alloy.CFG.Colors.SideMenuIconColor});
var favouriteIcon = fontIconLoader.getIcon("panacea","star",35,{color:Alloy.CFG.Colors.SideMenuIconColor});
var followIcon = fontIconLoader.getIcon("panacea","flow-cascade",35,{color:Alloy.CFG.Colors.SideMenuIconColor});
var transactionIcon = fontIconLoader.getIcon("panacea","history",35,{color:Alloy.CFG.Colors.SideMenuIconColor});
var transactionIcon2 = fontIconLoader.getIcon("panacea","history",35,{color:Alloy.CFG.Colors.transactionrowcolor});
var signOutIcon = fontIconLoader.getIcon("panacea","sign-out",35,{color:Alloy.CFG.Colors.SideMenuIconColor});
var cartIcon = fontIconLoader.getIcon("panacea","shopping-cart",35,{color:Alloy.CFG.Colors.IconWhite});

var appWrapper = null; 
var isSliderOpen = false;
var homeData = null;
var newsData;
var popData;

var shopCount = 2;
var shopsData;

// alert(Alloy.CFG.Languages.lblFavourite);

var users = Alloy.Collections.users;

$.imgSideMenu.image = sideMenuIcon;
$.btnCount.opacity = 0;

if(Alloy.isTablet) {
	$.imgSideMenu.width = 25;
	$.imgSideMenu.height = 25;
}

if (__.isiOS7Plus()) {
	$.AppWrapper.top = 20;
}

var beforeNodes = [
	{
		menuHeader : Alloy.CFG.Languages.appName,
		id : 0,
		title : Alloy.CFG.Languages.lblHome,
		image : homeIcon
	}, 
	{
		id : 1,
		title : Alloy.CFG.Languages.lblSwitchShop,
		image : switchShopIcon
	}, 
	{
		id : 5,
		title : Alloy.CFG.Languages.lblProfile,
		image : profileIcon
	} 
];

var afterNodes = [
	{
		menuHeader : Alloy.CFG.Languages.appName,
		id : 0,
		title : Alloy.CFG.Languages.lblHome,
		image : homeIcon
	}, 
	{
		id : 1,
		title : Alloy.CFG.Languages.lblSwitchShop,
		image : switchShopIcon
	}, 
	
	{
		id : 2,
		title : Alloy.CFG.Languages.lblFavourite,
		image : favouriteIcon
	}, 
	{
		id : 3,
		title : Alloy.CFG.Languages.lblFollow,
		image : followIcon
	}, 
	{
		id : 4,
		title : Alloy.CFG.Languages.lblTransactionHistory,
		image : transactionIcon
	}, 
	
	{
		id : 5,
		title : Alloy.CFG.Languages.lblProfile,
		image : profileIcon
	},
	{
		id : 6,
		title : Alloy.CFG.Languages.lbllogout,
		image : signOutIcon
	}  
];

function handleMenuClick(_event) {
	if ( typeof _event.row.id !== "undefined") {
		openScreen(_event.row.id);
	}
}


function openScreen(rowID) {
	if (rowID == 0) {
		loadShopView(shopsData);
	} else if (rowID == 1) {
		$.btnCount.opacity = 0;
		$.imgCart.opacity = 0;
		// loadShopView(shopsData);
		loadAllShops();
	} else if (rowID == 2) { // Favourite
		
		var params = {
				name : Alloy.CFG.Languages.lblFavourite
			};
			loadContentView("favouriteList", params);
//		
	} else if (rowID == 3) { // Favourite
		
		var params = {
				name : Alloy.CFG.Languages.lblFollow
			};
			loadContentView("followList", params);
		
	} else if (rowID == 4) { // Transaction
		loadingWindow.startLoading();
		var params = {
			name : Alloy.CFG.Languages.lblTransactionHistory
		};
		loadContentView("transactionList", params);
	} else if (rowID == 5) { 

		
		var params = {
				name : Alloy.CFG.Languages.lblProfile
		};
		loadContentView("userProfile", params);
	} else if (rowID == 6) {
		cleanUserModel();
		cleanBasketModel();
		loadShopView(shopsData);
		
		$.SlideMenu.clear();
		$.SlideMenu.Nodes.removeEventListener("click", handleMenuClick);
		initSlideMenu(beforeNodes);
	}
	closeMenu();
}

var cleanUserModel = function() {
	var user = users.get(Ti.App.Properties.getString("userId"));
	if (user != null) {
		user.destroy();
	}
	users.fetch();
	Ti.App.Properties.setString('userId', null);
	Ti.App.Properties.setString('delivery_address', null);
	Ti.App.Properties.setString('billing_address', null);
}; 

var cleanBasketModel = function() {
	Alloy.Collections.basket.deleteAll();
	Alloy.Collections.item_attribute.deleteAll();
	console.log("++++++++ Basket need to clean after user remove. ++++++++++");
	
	console.log("++++++++ After logout, Item Count at ItemDetail ++++++++++");
	var itemCountRefresh = Alloy.Globals.itemCountRefresh;
	itemCountRefresh();
	
	console.log("++++++++ After logout, Item Count at ItemList ++++++++++");
	var itemCountRefreshItemList = Alloy.Globals.itemCountRefreshItemList;
	itemCountRefreshItemList();
	
	console.log("++++++++ After logout, Item Count at Index ++++++++++");
	var itemCountRefreshIndex = Alloy.Globals.itemCountRefreshIndex;
	var params = {
		selected_shop_id : Ti.App.Properties.getString("selected_shop_id")
	};
	itemCountRefreshIndex(params);	
};

var loadContentView = function(viewName, args) {
	var contentView = Alloy.createController(viewName, args).getView();
	$.midContainer.removeAllChildren();
	$.midContainer.add(contentView);
	if(null != args) {
		$.mainTitle.text = args.name;
	} else {
		$.mainTitle.text = Alloy.CFG.Languages.appName;
	}
};

function initProcess() {
	appWrapper = $.AppWrapper;
	
	if(OS_IOS) {
		$.AppWrapper.addEventListener("swipe", function(_event) {
			if (_event.direction == "right") {
				openMenu();
				
			} else if (_event.direction == "left") {
				closeMenu();
			}
		});
	}
	
}

function openMenu() {
	appWrapper.animate({
		left : "200dp",
		right : "-200dp",
		duration : 250,
		curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
	});

	$.SlideMenu.Wrapper.animate({
		left : "0dp",
		right : "0dp",
		duration : 250,
		curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
	});
	
	isSliderOpen = true;
}

function closeMenu() {
	appWrapper.animate({
		left : "0dp",
		right : "0dp",
		duration : 250,
		curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
	});

	$.SlideMenu.Wrapper.animate({
		left : "-200dp",
		duration : 250,
		curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
	});

	isSliderOpen = false;
}

function openLeftMenu() {
	if (isSliderOpen) {
		closeMenu();
	} else {
		openMenu();
	}
}

function initSlideMenu(nodes) {
	
	$.SlideMenu.init({
		nodes : nodes,
		color : {
			headingBackground : "#000",
			headingText : "#FFF"
		}
	});

	$.SlideMenu.setIndex(0);
	$.SlideMenu.Nodes.addEventListener("click", handleMenuClick);

}

var callBackLoadAllCategories = function(args) {
	if(args.length != 0) {
		homeData = args;
		args.name = Alloy.CFG.Languages.appName;
		loadContentView('home', args);
	} else {
		dialogBox.loadCustomDialog("Mokets", Alloy.CFG.Languages.dataNotFound);
		loadingWindow.endLoading();
	}
};

var loadAllCategories = function() {
	var loaderArgs = {
		callbackFunction : callBackLoadAllCategories,
		url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getCategoriesWithThreeItems
	};
	loader.get(loaderArgs);
};

var updateFromVal = function() {
	var limit = Alloy.CFG.LimitOfPosts;
	var from = Ti.App.Properties.getInt("From");
	Ti.App.Properties.setInt("From", (limit+from));
};

var loadAllFeeds = function() {
	
	var limit = Alloy.CFG.LimitOfPosts;
	Ti.App.Properties.setInt("From", 0);
	
	var loaderArgs = {
		callbackFunction : callBackLoadAllFeeds,
		url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getFeeds + limit + '/from/0'
	};
	loader.get(loaderArgs);
};

var callBackLoadAllFeeds = function(argsFeeds) {
	if(argsFeeds != null) {
		if(argsFeeds.length > 0) {
			newsData = argsFeeds;
			updateFromVal();
			
			newsData.name = Alloy.CFG.Languages.lblNewsFeed;
			loadContentView("newsList", newsData);
		}
	}
};

var loadAllShops = function() {
	var loaderArgs = {
		callbackFunction : callBackLoadAllShops,
		url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getAllShops 
	};
	loader.get(loaderArgs);
};

var callBackLoadAllShops = function(argsShops) {
	if(argsShops != null) {
		if(argsShops.length > 0) {
			shopsData = argsShops;
			shopCount = argsShops.length;
			Ti.App.Properties.setString("currency_symbol", argsShops[0].currency_symbol);
			Ti.App.Properties.setString("currency_short_form", argsShops[0].currency_short_form);
			loadShopView(argsShops);
		}
	}
};

var clearView = function() {
	$.midContainer.removeAllChildren();
};

var loadShopView = function(shopsData) {
	clearView();
	if(shopCount == 1) {
		console.log(">>>>> Single Shop Mode <<<<<");
		Ti.App.Properties.setString("shop_mode","single");
		// console.log(shopsData[0].id);
		
		var params = {
			name  : Alloy.CFG.Languages.appName,
			shopData : shopsData 
		};
		loadContentView("singleShop", params);
		var shopArg = {
			selected_shop_id : shopsData[0].id
		};
		loadItemCount(shopArg);
		
	} else {
		console.log(">>>>> Multiple Shops Mode <<<<<");
		Ti.App.Properties.setString("shop_mode","multiple");
		var params = {
			name : Alloy.CFG.Languages.shops,
			shops : shopsData 
		};
		loadContentView("multipleShop", params);
	}
};

$.index.addEventListener('open', function() {
	
	loadingWindow.startLoading();
	initProcess();
	if(Ti.App.Properties.getString("userId") != null) {
		initSlideMenu(afterNodes);
	} else {
		initSlideMenu(beforeNodes);
	}
	
	loadAllShops();
});

Alloy.Globals.loadSingleShop = function(controller,selectedArgs) {
	$.imgCart.opacity = 1;
	$.imgCart.image = cartIcon;
	loadItemCount(selectedArgs);
	selectedArgs.name = Alloy.CFG.Languages.selectedShop;
	loadContentView(controller,selectedArgs);
};

Ti.App.addEventListener('refreshMenu',function(e) {
	$.SlideMenu.clear();
	$.SlideMenu.Nodes.removeEventListener("click", handleMenuClick);
	initSlideMenu(afterNodes);
});

var loadItemCount = function(selectedArgs)
{
	var myModel =  Alloy.createCollection('basket');
	myModel.fetch();
	var data = myModel.where({"shop_id": parseInt(selectedArgs.selected_shop_id)});
	var itemCount = data.length;
	if(itemCount >= 1){
		$.btnCount.title = itemCount.toString();
		$.btnCount.opacity = 1;
		$.imgCart.image = cartIcon;
		$.imgCart.opacity = 1;
	} else {
		$.btnCount.opacity = 0;
		$.imgCart.opacity = 0;
	}
};


var openBasket = function()
{
	var myModel =  Alloy.createCollection('basket');
	myModel.fetch();
	var data = myModel.where({"shop_id" : parseInt(Ti.App.Properties.getString("selected_shop_id"))});
	console.log("Selected shop id : " + Ti.App.Properties.getString("selected_shop_id"));
	if(data.length >= 1){
		var contentView = Alloy.createController("basket").getView();
		psAnimation.in(contentView);
	} else {
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.noItemsInsideBasket);
	}
};


Alloy.Globals.itemCountRefreshIndex = function(args) {
	console.log("Fired >>>>>>>>> Item Count Refresh At index.js");
	$.imgCart.image = cartIcon;
	$.imgCart.opacity = 1;
	loadItemCount(args);
};


$.index.open();
