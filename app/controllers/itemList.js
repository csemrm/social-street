var psAnimation = require('animation');
var __ = require("platformSupport");
var args = arguments[0] || {};
var alloyAnimation = require('alloy/animation');
var fontIconLoader = require("icomoonlib");
var dialogBox = require("psdialog");
	
var initUI = function(subCats) {
	var contentView = null;
	for(var i=0; i<subCats.length; i++) {
		
		var params = {
			subCatImage   : Alloy.CFG.Urls.imagePathURL + subCats[i].cover_image_file,
			subCatId      : subCats[i].id,
			subCatName    : subCats[i].name,
		};
		contentView = Alloy.createController("itemListLayout", params).getView();
		
		var params = {
			view : {
				tabName : subCats[i].name,
				tabView : contentView
			}
		};
		$.PSTabMenu.addView(params);
		//$.PSTabMenu.moveTo(args.selectedSubCategoryIndex);
		$.PSTabMenu.moveTo(0);
		
	}
};


Ti.App.addEventListener('shakeSubCategoryTitle',function(e) {
	alloyAnimation.shake($.lblSubCategoryTitle, 200, null);
});

var init = function() {
	
	var params = {
		tabWidth : 100,
		tabPadding : 5,
		tabHeight : 25,
		tabStyle : {
			font : {
				fontSize : 13,
				fontFamily: "Monda-Regular"	
			},
			focusColor : '#212121', 
			color : '#5c5b5b',
			backgroundColor : '#fff',
			barColor : 'red'
		}
	};
	
	// need to init first before add views
	$.PSTabMenu.init(params);	
	
	initUI(args.subCats);
	
	
	if (__.isiOS7Plus()) {
		$.AppWrapper.top = 20;
	}
	
	var backIcon = fontIconLoader.getIcon("panacea","back",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgBack.image = backIcon;
	
	var cartIcon = fontIconLoader.getIcon("panacea","shopping-cart",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgCart.image = cartIcon;
	
	$.mainTitle.text = args.selectedMainCatName;
	
	loadItemCount();
};

var loadItemCount = function() {
	var myModel =  Alloy.createCollection('basket');
	myModel.fetch();
	var data = myModel.where({"shop_id": parseInt(Ti.App.Properties.getString("selected_shop_id"))});
	
	//myModel.fetch({query:"SELECT * FROM basket"});
	
	var itemCount = data.length;
	if(itemCount >= 1){
		$.btnCount.title = itemCount.toString();
		$.btnCount.opacity = 1;
	} else {
		$.btnCount.opacity = 0;
	}
};


var openBasket = function() {
	var myModel =  Alloy.createCollection('basket');
	myModel.fetch();
	var data = myModel.where({"shop_id": parseInt(Ti.App.Properties.getString("selected_shop_id"))});
	
	//myModel.fetch({query:"SELECT * FROM basket"});
	
	if(data.length >= 1){
		var contentView = Alloy.createController("basket").getView();
		psAnimation.in(contentView);
	} else {
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.noItemsInsideBasket);
	}
};

var closeWindow = function() {
	psAnimation.out($.itemList);
};

Alloy.Globals.closeItemList = function() {
	closeWindow();
};

Alloy.Globals.itemCountRefreshItemList = function() {
	console.log("Fired >>>>>>>>> Item Count Refresh At itemlist.js");
	loadItemCount();
};



init();
	
