var args = arguments[0] || {};
var psAnimation = require('animation');
var __ = require('platformSupport');
var fontIconLoader = require("icomoonlib");
var dialogBox = require("psdialog");
var loadingWindow = require('loadingWindow');
var loader = require("loader");
var basket = Alloy.Collections.basket;
var myBasket;
var totalAmount=0;
var checkout_shop_id=0;
var attributeString="";

if (__.isiOS7Plus()) {
	$.AppWrapper.top = 20;
}

var init = function() {
     loadIcon();
     $.lblPaypal.text = Alloy.CFG.Languages.paypalCheckoutMessage;
     psAnimation.slowlyAppear($.lblPaypal);
};

var loadIcon = function() {
	var backIcon = fontIconLoader.getIcon("panacea","back",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgBack.image = backIcon;
};

var closeWindow = function() {
	psAnimation.out($.paypal);
};

var callBackSuccessPayment = function(respData) {
	loadingWindow.endLoading();
	if(respData != null) {
		if(respData.success != ""){
			// Assume that order transactions has been inserted into server DB
			// Need to close 'paypal' page and open success page
			psAnimation.close($.paypal);
			
			var contentView = Alloy.createController("paymentSuccess").getView();
			psAnimation.in(contentView);
			
			console.log("+++++ Already Clean Basket ++++++");
			cleanBasket();
			
			console.log("+++++ Already Clean Item Attribute ++++++");
			cleanAttribute();

			console.log("++++++++ After Payemnt Suucess, Refresh Basket Item Count at Item Detail ++++++++++");
			var itemCountRefresh = Alloy.Globals.itemCountRefresh;
			itemCountRefresh();
			
			console.log("++++++++ After Payemnt Suucess, Refresh Basket Item Count at Item List ++++++++++");
			var itemCountRefreshItemList = Alloy.Globals.itemCountRefreshItemList;
			itemCountRefreshItemList();
			
			console.log("++++++++ After Payemnt Suucess, Refresh Basket Item Count at Index ++++++++++");
			var itemCountRefreshIndex = Alloy.Globals.itemCountRefreshIndex;
			var params = {
				selected_shop_id : checkout_shop_id
			};
			itemCountRefreshIndex(params);
			
			console.log("+++++ Already Close All Window From Background +++++++++ ");
			var closeCheckout = Alloy.Globals.closeCheckout;
			closeCheckout();
			var closeBasket = Alloy.Globals.closeBasket;
			closeBasket();
			var closeItemDetail = Alloy.Globals.closeItemDetail;
			closeItemDetail();
			var closeItemList = Alloy.Globals.closeItemList;
			closeItemList();
			var closePaymentOption = Alloy.Globals.closePaymentOption;
			closePaymentOption();
		}
	}
};

var cleanBasket = function() {
	//Alloy.Collections.basket.deleteAll();
	Alloy.Collections.basket.deleteRecord({
	  query : {
	     sql : "WHERE shop_id=?",
	     params : checkout_shop_id
	   }
	 });
};

var cleanAttribute = function() {
	Alloy.Collections.item_attribute.deleteRecord({
	  query : {
	     sql : "WHERE shop_id=?",
	     params : checkout_shop_id
	   }
	 });
};

var itemGotAttribute = function(item_id,shop_id) {
	var item_attribute =  Alloy.createCollection('item_attribute');
	item_attribute.fetch();
	var data = item_attribute.where({"item_id" : item_id});
	for(var i=0; i < data.length; i++)
	{
		if(data.length-1 == i){
			attributeString += data[i].get("header") + " : " + data[i].get("name");
		} else {
			attributeString += data[i].get("header") + " : " + data[i].get("name") +", ";
		}
	}
	console.log("Before Return attributeString : " + attributeString);
	return attributeString;
};

$.paypal.addEventListener('open', function() {
    var Paypal = require('ti.paypal');
    var u = Ti.Android != undefined ? 'dp' : 0;
    var status = Ti.UI.createLabel({
        top: 20 + u, height: 50 + u, color: '#333',
        text: 'Loading, please wait...'
    });
    $.paypal.add(status);
 
    var button;
    function addButtonToWindow() {
        if (button) {
            $.paypal.remove(button);
            button = null;
        }
        button = Paypal.createPaypalButton({
            width: 194 + u, height: 37 + u,
            buttonStyle: Paypal.BUTTON_194x37, 
            top: 20 + u,
 
            language: 'en_US',
            textStyle: Paypal.PAYPAL_TEXT_PAY, 
 
            appID: Ti.App.Properties.getString("selected_shop_paypal_appid_live"),
            
            //paypalEnvironment: Paypal.PAYPAL_ENV_NONE, // Sandbox, None or Live
 			paypalEnvironment: eval(Ti.App.Properties.getString("selected_shop_paypal_environment")), // Sandbox, None or Live
 			
            feePaidByReceiver: false,
            enableShipping: false, 
 			
 			//Old Method As Read From Config File
 			/*	
             payment: { 
	            paymentType : Paypal.PAYMENT_TYPE_SERVICE, 
	            subtotal    : args.amount, 
	            tax         : 0,
	            shipping    : 0,
	            currency    : Alloy.CFG.Payment.Paypal_Currency,
	            recipient   : Alloy.CFG.Payment.Paypal_Recipient_Email,
	            customID    : Alloy.CFG.Payment.Paypal_CustomerID,
	            invoiceItems: [
	                { name: 'Total Amount', totalPrice: args.amount, itemPrice: args.amount, itemCount: 1 }
	            ],
	            ipnUrl       : Alloy.CFG.Payment.Paypal_IpnUrl,
	            merchantName : Alloy.CFG.Payment.Pyapal_MerchantName,
	            memo         : Alloy.CFG.Payment.Paypal_Memo
	        }
	        */
	      	 payment: { 
	            paymentType : Paypal.PAYMENT_TYPE_SERVICE, 
	            subtotal    : args.amount, 
	            tax         : 0,
	            shipping    : 0,
	            currency    : Ti.App.Properties.getString("selected_shop_paypal_currency"),
	            recipient   : Ti.App.Properties.getString("selected_shop_paypal_email"),
	            customID    : Ti.App.Properties.getString("selected_shop_paypal_customerid"),
	            invoiceItems: [
	                { name: 'Total Amount', totalPrice: args.amount, itemPrice: args.amount, itemCount: 1 }
	            ],
	            ipnUrl       : Ti.App.Properties.getString("selected_shop_paypal_ipnurl"),
	            merchantName : Ti.App.Properties.getString("selected_shop_paypal_merchantname"),
	            memo         : Ti.App.Properties.getString("selected_shop_paypal_memo")
	        }
	       
        });
 
        // Events available
        button.addEventListener('paymentCancelled', function (e) {
            //alert('Payment cancelled. Please try again!');
            //addButtonToWindow();
            psAnimation.close($.paypal);
			var params = {
				checkout_shop_id : checkout_shop_id
			};
			var contentView = Alloy.createController("paymentCancel",params).getView();
			psAnimation.in(contentView);
        });
        button.addEventListener('paymentSuccess', function (e) {
            console.log('Payment successfull. Please get your transaction id : ' + e.transactionID);
            //Assume that Payment is success from Paypal, Need to update at Server Database
            //Get all records from local model and send to API
			
				myBasket =  Alloy.createCollection('basket');
				myBasket.fetch();
				//myBasket.fetch({query:"SELECT * FROM basket"});
				var data = myBasket.where({"shop_id": parseInt(Ti.App.Properties.getString("selected_shop_id"))});
	
				console.log(" Total Records To Send Server : " + data.length);
			    
				var transactions = {};
				var orders = [];
				
			    for(var i=0; i < data.length; i++)
				{
				    checkout_shop_id = data[0].get("shop_id");
				    
				    orders.push({ 
				        "item_id"          		 : data[i].get("item_id"),
				        "shop_id"          		 : data[i].get("shop_id"),
				        "unit_price"       		 : data[i].get("unit_price"),
				        "discount_percent" 		 : data[i].get("discount_percent") || 0,
				        "name"             		 : data[i].get("name"),
				        "qty"              		 : data[i].get("qty"), 
				        "user_id"                : data[i].get("user_id"),
				        "paypal_trans_id" 	     : e.transactionID,
				        "delivery_address"       : args.delivery_address,
				        "billing_address"        : args.billing_address,
				        "order_total_amount"     : args.amount,
				        "basket_item_attribute"  : itemGotAttribute(data[i].get("item_id"),data[i].get("shop_id")),
				    	"payment_method"         : "paypal",
				    	"email"                  : args.email,
				    	"phone"                  : args.phone
				    });
				    
				    attributeString = "";
				}
			    transactions.orders = orders;
			    console.log(JSON.stringify(transactions));  
			    loadingWindow.startLoading();
			
				var apiArgs = {
					callbackFunction : callBackSuccessPayment,
					payload          : transactions,
					url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postTransactionsData
				};
				
				loader.post(apiArgs);
			             
        });
        
        button.addEventListener('paymentError', function (e) {
            console.log("Payment Error Code " + e.errorCode);
            psAnimation.close($.paypal);
			var params = {
				checkout_shop_id : checkout_shop_id
			};
			var contentView = Alloy.createController("paymentError",params).getView();
			psAnimation.in(contentView);
            
        });
 
        button.addEventListener('buttonDisplayed', function () {
            $.paypal.remove(status);
        });
 
        button.addEventListener('buttonError', function () {
 
        });
 
        $.paypalScrollView.add(button);
    }
    addButtonToWindow();   
    
    init();  
    
});

