var __ = require('platformSupport');
var loadingWindow = require('loadingWindow');
var strings = require("strings");
var fontIconLoader = require("icomoonlib");
var psAnimation = require('animation');
var args = arguments[0] || {};

var gotoShopFromRight = function() {
	var loadSingleShop = Alloy.Globals.loadSingleShop;
	var params = {
		selected_shop_id : args.shopData.id
	};
	args.selected_shop_id = args.shopData.id;
	loadSingleShop('singleShop',args);
};

var shopImageClick = function() {
};

var init = function() {
	if(args!=null) {
		if(args.shopData != null) {	
			var scale = {
				width  : args.shopData.cover_image_width,
				height : args.shopData.cover_image_height
			};
	
			var viewScale = {
				width : args.ITEM_BIG_WIDTH,
				height : args.ITEM_BIG_HEIGHT
			};
			scale = __.getGridPhotoSize(scale, viewScale);
			
			$.productCountView.width = args.ITEM_SMALL_WIDTH;
			$.productCountView.height = args.ITEM_SMALL_HEIGHT;
			
			$.categoryCountView.width = args.ITEM_SMALL_WIDTH;
			$.categoryCountView.height = args.ITEM_SMALL_HEIGHT;
			
			$.followerCountView.width = args.ITEM_SMALL_WIDTH;
			$.followerCountView.height = args.ITEM_SMALL_HEIGHT;
			
			$.shadow.width = scale.width;
			$.lblShopName.width = scale.width;
			
			$.shopPhotoView.width = args.ITEM_BIG_WIDTH;
			$.shopPhotoView.height = args.ITEM_BIG_HEIGHT;
			
			$.imgShop.width = scale.width;
			$.imgShop.height = scale.height;
			$.lblShopName.text = args.shopData.name;
			$.lblItemCount.text = args.shopData.item_count;
			$.lblCategoryCount.text = args.shopData.category_count;
			$.lblFollowerCount.text = args.shopData.follow_count;
			$.imgShop.image = Alloy.CFG.Urls.imagePathURL + args.shopData.cover_image_file;
			$.lblShopDescription.text = args.shopData.description;		
			
			if(Alloy.isTablet) {
				$.lblCategoryCount.top = 25;
				__.setNormalFontForTablet($.lblCategoryCount,20);
				__.setNormalFontForTablet($.lblCategories,20);
				
				$.lblFollowerCount.top = 25;
				__.setNormalFontForTablet($.lblFollowerCount,20);
				__.setNormalFontForTablet($.lblFollowers,20);		
				
				$.lblItemCount.top = 25;
				__.setNormalFontForTablet($.lblItemCount,20);
				__.setNormalFontForTablet($.lblItems,20);		
				
				__.setNormalFontForTablet($.lblShopName,20);
				__.setNormalFontForTablet($.lblShopDescription,15);
				
				$.shopDescriptionView.height = 80;
			}
			
			if(!OS_IOS) {
				$.lblShopDescription.text = __.readMoreText(150,args.shopData.description);
				$.lblShopDescription.left = 5;
				
				if(!Alloy.isTablet) {
					$.lblCategoryCount.top = 13;
					$.lblFollowerCount.top = 13;
					$.lblItemCount.top = 13;
				}
				
				$.lblFollowerCount.setColor("#000");
				$.lblCategoryCount.setColor("#000");
				$.lblItemCount.setColor("#000");
				
			}
			
			loadLanguage();
			
		}
	}
};

var loadLanguage = function() {
	$.lblItems.text = Alloy.CFG.Languages.lblItems;
	$.lblCategories.text = Alloy.CFG.Languages.lblCategories;
	$.lblFollowers.text = Alloy.CFG.Languages.lblFollowers;
};


var applyAnimation = function() {
	psAnimation.slowlyAppear($.shopPhotoView);
	psAnimation.slowlyAppear($.productCountView);
	psAnimation.slowlyAppear($.categoryCountView);
	psAnimation.slowlyAppear($.followerCountView);
	psAnimation.slowlyAppear($.shopDescriptionView);
};
init();
