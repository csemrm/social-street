var args = arguments[0] || {};
var psAnimation = require('animation');
var __ = require('platformSupport');
var fontIconLoader = require("icomoonlib");
var myBasket;
var attributeString = "";

if (__.isiOS7Plus()) {
	$.AppWrapper.top = 20;
}


var init = function() {
     $.basketListView.removeAllChildren();
     loadBasket();
     loadIcon();
};


var closeWindow = function() {
	psAnimation.out($.basket);
};

var loadBasket = function() {
	myBasket =  Alloy.createCollection('basket');
	myBasket.fetch();
	var data = myBasket.where({"shop_id": parseInt(Ti.App.Properties.getString("selected_shop_id"))});
	console.log(" Total Records : " + data.length);
    
    for(var i=0; i < data.length; i++)
	{
		
		var params = {
			id              : data[i].get("id"),
			item_id         : data[i].get("item_id"),
			shop_id         : data[i].get("shop_id"),
			user_id         : data[i].get("user_id"),
			name            : data[i].get("name"),
			description     : data[i].get("description"),
			unit_price      : data[i].get("unit_price"),
			qty             : data[i].get("qty"),
			last_updated_at : data[i].get("last_updated_at"),
			image           : data[i].get("image") || "/images/defaultBasket.png",
			currency_sign   : data[i].get("currency_sign"),
			basket_item_attribute  : itemGotAttribute(data[i].get("item_id"),data[i].get("shop_id"))
		};
		attributeString = "";
		params.fromWhere = "basket";
		oneRow = Alloy.createController('basketRow', params).getView();
		$.basketListView.add(oneRow);
	}   
			
};

var itemGotAttribute = function(item_id,shop_id) {
	var item_attribute =  Alloy.createCollection('item_attribute');
	item_attribute.fetch();
	var data = item_attribute.where({"item_id" : item_id});
	console.log("length >>>> " + data.length);
	for(var i=0; i < data.length; i++)
	{
		if(data.length-1 == i){
			console.log("if  >>> " + data[i].get("header") + " : " + data[i].get("name"));
			attributeString += data[i].get("header") + " : " + data[i].get("name");
		} else {
			console.log("el  >>> " + data[i].get("header") + " : " + data[i].get("name") +", ");
			attributeString += data[i].get("header") + " : " + data[i].get("name") +", ";
		}
	}
	console.log("Before Return attributeString : " + attributeString);
	return attributeString;
};

var loadIcon = function() {
	var backIcon = fontIconLoader.getIcon("panacea","back",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgBack.image = backIcon;
};


var goPaymentOption = function() {
	var contentView = Alloy.createController("paymentOption").getView();
	psAnimation.in(contentView);
};

Alloy.Globals.refreshBasket = function() {
	init();
};

Alloy.Globals.closeBasket = function() {
	console.log("Fired >>>>>>>>> closeWindows At basket.js");
	psAnimation.close($.basket);
};

var loadLanguage = function()
{
	$.mainTitle.text = Alloy.CFG.Languages.lblBasket;
	$.lblPaymentOption.text = Alloy.CFG.Languages.lblPaymentOption;
};

init();
