var args = arguments[0] || {};
var __ = require('platformSupport');
var psAnimation = require('animation');
var fontIconLoader = require("icomoonlib");
var loader = require("loader");
var validation = require("validationRules");
var dialogBox = require("psdialog");
var users = Alloy.Collections.users;
var changeFlag = 0;
var loadingWindow = require('loadingWindow');

loadingWindow.endLoading();

if (__.isiOS7Plus()) {
	$.AppWrapper.top = 20;
}

var closeWindow = function() {
	psAnimation.out($.userLogin);
};

var openUserLogin = function() {
	if(validationChecking()){
		if(Titanium.Network.online == true){
			var payloadJSON = {"email": $.txtEmail.value.toLowerCase(), "password":$.txtPassword.value};
			loadingWindow.startLoading();
			var loaderArgs = {
				callbackFunction : loginProcess,
				url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postUserLogin,
				payload          : payloadJSON
			}; 
			loader.post(loaderArgs);
		}else{
			dialogBox.loadCustomDialog("Log In", Alloy.CFG.Languages.offlineMessage);
		}
	}
};

var loginProcess = function(respData) {
	console.log("Login Success. Need to store at Local DB.");
	if(respData.error == null) {
		if(respData != null) {
			Ti.App.fireEvent('refreshMenu');
			
			if(respData.profile_photo != "") {
				if(Titanium.Network.online == true) {
					var downloadArgs = {
						callbackFunction : downloadFileSuccess,
						urlDownload      : Alloy.CFG.Urls.imagePathURL + respData.profile_photo,
						fileName         : respData.profile_photo
					};
					
					loader.downloadFileToApp(downloadArgs);
					
				} else {
					console.log(">>> Lacking Connection During Profile Photo Download");	
				}	
				
			}
			
			if(respData.background_photo != "") {
				if(Titanium.Network.online == true) {
					var downloadArgs = {
						callbackFunction : downloadFileSuccess,
						urlDownload      : Alloy.CFG.Urls.imagePathURL + 	respData.background_photo,
						fileName         : respData.background_photo
					};
					loader.downloadFileToApp(downloadArgs);
				} else {
					console.log(">>> Lacking Connection During Profile Background Download");	
				}	
				
			}
			
			var userModel = Alloy.createModel('users',{
				id               : respData.id,
				username         : respData.username,
				email            : respData.email,
				about_me         : respData.about_me,
				is_banned        : respData.is_banned,
				profile_photo    : respData.profile_photo,
				background_photo : respData.background_photo,
				delivery_address : respData.delivery_address,
				billing_address  : respData.billing_address
			});
			
			users.add(userModel);
			userModel.save();
			users.fetch();
			
			Ti.App.Properties.setString('userId', respData.id);
			Ti.App.Properties.setString('delivery_address', respData.delivery_address);
			Ti.App.Properties.setString('billing_address', respData.billing_address);
			Ti.App.Properties.setString('email', respData.email);
		  
		    closeWindow();
		    
		    if(args.item_id == 0) {
				closeWindow();
			} else {
				var contentView = Alloy.createController("review", args).getView();
		    	psAnimation.in(contentView);
		    }
		    
		    loadingWindow.endLoading();
		}
	}else{
		loadingWindow.endLoading();
		dialogBox.loadCustomDialog('Log In', respData.error.message);
	}
};

function downloadFileSuccess(fileName) {
	console.log(">>> Download Image Successful : " + fileName);
}


var validationChecking = function() {
	
	if($.txtEmail.value=="") {
		validation.validationFailAction($.txtEmail); 
    	changeFlag = 0;
    	return false;
	} else {
		
		if(!validation.validateEmail($.txtEmail.value)) {
			validation.validationFailAction($.txtEmail); 
    		changeFlag = 0;
    		return false;
		}
	}
	
	if($.txtPassword.value=="") {
		validation.validationFailAction($.txtPassword); 
    	changeFlag = 0;
    	return false;
	}
	return true;
};


var openUserRegister = function() {
	closeWindow();
	loadingWindow.startLoading();
	var params = {
		item_id : args.item_id,
		loadReview : args.loadReview
	};
	var contentView = Alloy.createController("userRegister",params).getView();
	psAnimation.in(contentView);
};

var openForgotPassword = function() {
	closeWindow();
	loadingWindow.startLoading();
	var params = {
		item_id : args.item_id
	};
	var contentView = Alloy.createController("userForgot",params).getView();
	psAnimation.in(contentView);
};

$.txtEmail.addEventListener('change',function(e) {
	if(changeFlag==0) {
		validation.backToNormal($.txtEmail);
		changeFlag=1;
	}
});


$.txtPassword.addEventListener('change',function(e) {
	if(changeFlag==0) {
		validation.backToNormal($.txtPassword);
		changeFlag=1;
	}
});

var loadIcon = function() {
	var emailIcon = fontIconLoader.getIcon("panacea","envelope",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgEmailLogin.image = emailIcon;
	
	var passwordIcon = fontIconLoader.getIcon("panacea","lock",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgPasswordLogin.image = passwordIcon;
	
	var backIcon = fontIconLoader.getIcon("panacea","back",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgBack.image = backIcon;
};

var loadLanguage = function() {
	$.lblEmail.text = Alloy.CFG.Languages.lblEmail;
	$.lblPassword.text = Alloy.CFG.Languages.lblPassword;
	$.btnLogin.title = Alloy.CFG.Languages.btnLogin;
	$.btnRegister.title = Alloy.CFG.Languages.btnRegister;
	$.btnForgot.title = Alloy.CFG.Languages.btnForgot;
	$.mainTitle.text = Alloy.CFG.Languages.lblLogin;
};


var init = function() {
	loadIcon();
	loadLanguage();
	applyAnimation();
	
	if(!OS_IOS) {
		$.lblEmail.top = 13;
		$.lblPassword.top = 17;
		
		$.txtEmail.blur();
		$.txtPassword.blur();
	}
};

var applyAnimation = function() {
	psAnimation.slowlyAppear($.userLoginView);
};

$.userLogin.addEventListener('open', function(){
	loadingWindow.endLoading();	
	init();
});




