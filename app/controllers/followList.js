var __ = require('platformSupport');
var loadingWindow = require('loadingWindow');
var myAnimation = require('animation');
var loader = require("loader");
var strings = require("strings");
var args = arguments[0] || {};
var fontIconLoader = require("icomoonlib");

var FIXED_ITEM_BIG_WIDTH = 213;
var FIXED_ITEM_BIG_HEIGHT = 150;
var FIXED_ITEM_SMALL_WIDTH = 107;
var FIXED_ITEM_SMALL_HEIGHT = 75;
var FIXED_SCREEN_WIDTH = 320; 
var ITEM_BIG_HEIGHT = 0;
var ITEM_BIG_WIDTH = 0;
var ITEM_SMALL_HEIGHT = 0;
var ITEM_SMALL_WIDTH = 0;
var SCREEN_WIDTH = 0;
var SCREEN_HEIGHT = 0;

var loadShopsListLayout = function(shopData) {
	var params = {
		ITEM_BIG_WIDTH : ITEM_BIG_WIDTH,
		ITEM_BIG_HEIGHT : ITEM_BIG_HEIGHT,
		ITEM_SMALL_WIDTH : ITEM_SMALL_WIDTH,
		ITEM_SMALL_HEIGHT : ITEM_SMALL_HEIGHT,
		shopData          : shopData
	};
	var layout = Alloy.createController('shopsListForFeedLayout', params).getView();
	return layout;
};


var calculateSize = function() {
	SCREEN_WIDTH =  __.getScreenWidth();
	SCREEN_HEIGHT = __.getScreenHeight();
	ITEM_BIG_WIDTH = ((SCREEN_WIDTH / FIXED_SCREEN_WIDTH) * FIXED_ITEM_BIG_WIDTH) - 1;
	ITEM_BIG_HEIGHT = ((ITEM_BIG_WIDTH / FIXED_ITEM_BIG_WIDTH ) * FIXED_ITEM_BIG_HEIGHT)-1;
	ITEM_SMALL_WIDTH = ((SCREEN_WIDTH / FIXED_SCREEN_WIDTH) * FIXED_ITEM_SMALL_WIDTH)-1;
	ITEM_SMALL_HEIGHT = ((ITEM_SMALL_WIDTH / FIXED_ITEM_SMALL_WIDTH) * FIXED_ITEM_SMALL_HEIGHT)-1;
};

var init = function() {
	loadFollowShopsByUser();
};

var loadFollowShopsByUser = function() {
	if(Titanium.Network.online == true) {
		
		if(Ti.App.Properties.getString("userId")) {
			loadingWindow.startLoading();
			var loaderArgs = {
				callbackFunction : callBackLoadFollowShopsByUser,
				url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getFollowShopsByUser +  Ti.App.Properties.getString("userId")
			};
			loader.get(loaderArgs);
		} else {
			dialogBox.loadCustomDialog("Follow", Alloy.CFG.Languages.needForFollowMesssage);
		}
		
	} else {
		dialogBox.loadCustomDialog("Follow", Alloy.CFG.Languages.offlineMessage);
	}
};

var callBackLoadFollowShopsByUser = function(argsShops) {
	loadingWindow.endLoading();
	if(argsShops != null) {
		
		if(argsShops.error) {
			$.dataNotFoundView.opacity = 1;
			$.dataNotFoundView.height = Ti.UI.SIZE;
			var notFoundIcon = fontIconLoader.getIcon("panacea","megaphone",35,{color:Alloy.CFG.Colors.MainColor});
			$.imgNotFound.image = notFoundIcon;
			$.lblNotFoundMessage.text = "Oops! Data Not Found From Server.";
		} else {
			if(argsShops.length > 0) {
				calculateSize();
		
				$.shopScrollView.contentWidth = SCREEN_WIDTH;
				$.shopScrollView.width = SCREEN_WIDTH;
				$.shopScrollView.height = Ti.UI.FILL;
			
				for(var i=0;i < argsShops.length; i++) {
					$.shopScrollView.add(loadShopsListLayout(argsShops[i]));
				}
			}
		}
		
	
	}
};

init();