var __ = require('platformSupport');
var loadingWindow = require('loadingWindow');
var strings = require("strings");
var fontIconLoader = require("icomoonlib");
var psAnimation = require('animation');
var args = arguments[0] || {};

var gotoFeedList = function() {
	loadingWindow.startLoading();
	var contentView = Alloy.createController("feedsList", args).getView();
	psAnimation.in(contentView);
};

var init = function() {
	if(args!=null) {
		if(args.shopData != null) {
			var scale = {
				width  : args.shopData.cover_image_width,
				height : args.shopData.cover_image_height
			};
			
			var viewScale = {
				width : args.ITEM_BIG_WIDTH,
				height : args.ITEM_BIG_HEIGHT
			};
			scale = __.getGridPhotoSize(scale, viewScale);
			
			$.feedCountView.width = args.ITEM_SMALL_WIDTH;
			$.feedCountView.height = args.ITEM_SMALL_HEIGHT;
			
			$.followerCountView.width = args.ITEM_SMALL_WIDTH;
			$.followerCountView.height = args.ITEM_SMALL_HEIGHT;
			
			$.shadow.width = scale.width;
			$.lblShopName.width = scale.width;
			
			$.shopPhotoView.width = args.ITEM_BIG_WIDTH;
			$.shopPhotoView.height = args.ITEM_BIG_HEIGHT;
			
			$.imgShop.width = scale.width;
			$.imgShop.height = scale.height;
			$.lblShopName.text = args.shopData.name;
			$.lblFeedCount.text = args.shopData.feed_count;
			$.lblFollowerCount.text = args.shopData.follow_count;
			$.imgShop.image = Alloy.CFG.Urls.imagePathURL + args.shopData.cover_image_file;
			$.lblShopDescription.text = args.shopData.description;
			
			if(Alloy.isTablet) {
				$.lblFollowerCount.top = 25;
				__.setNormalFontForTablet($.lblFollowerCount,20);
				__.setNormalFontForTablet($.lblFollowers,20);		
				
				$.lblFeedCount.top = 25;
				__.setNormalFontForTablet($.lblFeedCount,20);
				__.setNormalFontForTablet($.lblFeeds,20);		
				
				__.setNormalFontForTablet($.lblShopName,20);
				__.setNormalFontForTablet($.lblShopDescription,15);
				
				$.shopDescriptionView.height = 80;
				
			}
			
			if(!OS_IOS) {
				$.lblFeedCount.color = "#000";
				$.lblFollowerCount.color = "#000";
				
				$.lblShopDescription.text = __.readMoreText(150,args.shopData.description);
			}
			
			applyAnimation();

		}
	}	
		
};

var applyAnimation = function() {
	psAnimation.slowlyAppear($.shopFeedContainer);
};

var loadLanguage = function() {
	$.lblFeeds.text = Alloy.CFG.Languages.lblFeeds;
	$.lblFollowers.text = Alloy.CFG.Languages.lblFollowers;
};

init();
