var args = arguments[0] || {};
var strings = require('strings');
var __ = require('platformSupport');
var fontIconLoader = require("icomoonlib");
var loadingWindow = require('loadingWindow');
var psAnimation = require('animation');
var loader = require("loader");
var basket = Alloy.Collections.basket;
var dishImage;
var where;
var qty;
var basketId;
var attributesString="";


var init = function()
{
		
	if(where == "basket") {
		$.imgDish.image = Alloy.CFG.Urls.imagePathURL + dishImage;
	} else {
	
		if(args.dish_images.length == 0) {
			$.imgDish.image = "/images/defaultBasket.png";
		} else {
			$.imgDish.image = Alloy.CFG.Urls.imagePathURL + args.dish_images[0].path;
		}
	}
	
	$.lblItemTitle.text = args.name;
	$.lblItemDesc.text = args.description;
	//$.lblItemPrice.text = Alloy.CFG.Languages.lblPrice + args.unit_price + " " + Ti.App.Properties.getString("currency_symbol");
	
	//$.lblItemPrice.text = Alloy.CFG.Languages.lblPrice + args.unit_price + " " + Ti.App.Properties.getString("currency_symbol");
	$.lblItemPrice.text = Alloy.CFG.Languages.lblPrice+Ti.App.Properties.getString("currency_symbol") + args.unit_price ;
	//$.lblItemPrice.text = Alloy.CFG.Languages.lblPrice + args.unit_price ;
	if(attributesString != "") {
		$.lblAttribute.text = "Attribute (" + attributesString + ")";
	} else {
		$.lblAttribute.opacity = 0;
		$.lblAttribute.height = 0;
	}
	$.txtQTY.value = qty; //From Local Model
	$.txtQTY.dishId = args.id;
	$.txtQTY.orgQty = qty;
	$.txtQTY.basketId = basketId;
	
	$.lblQty.text = Alloy.CFG.Languages.lblQtyDivider;
	$.lblDivider.text = Alloy.CFG.Languages.lblDivider;
	
	if(!OS_IOS) {
		$.txtQTY.returnKeyType = Titanium.UI.RETURNKEY_DONE;
	}
	
	if(OS_IOS) {
		var flexSpace = Titanium.UI.createButton({
		    systemButton : Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
		});
		
		var btnDone = Titanium.UI.createButton({    
		    title : 'Update',
		    width : 67,
		    height : 32,
		});
		btnDone.addEventListener('click',function(e){
		    $.txtQTY.blur();
		    
		    console.log("New QTY " + $.txtQTY.value);
		    console.log("Org QTY " + $.txtQTY.orgQty);
		    console.log("Dish Id " + $.txtQTY.dishId);
		    console.log("Basket Id " + $.txtQTY.basketId);
		    
		    //Need to update if QTY is change
		    if($.txtQTY.value != $.txtQTY.orgQty) {
		    	basket.fetch();
				basket.get($.txtQTY.basketId).set({
					qty : parseInt($.txtQTY.value)
				}).save();
				
				basket.fetch();
				console.log("+++++++++++++ After Update QTY +++++++++++");    
				Ti.API.info(basket.toJSON());
		    }
		    
		});
		 
		var btnCancle = Titanium.UI.createButton({
		    title : 'Cancel',
		    width : 67,
		    height : 32,
		});
		
		btnCancle.addEventListener('click',function(e){
		    $.txtQTY.blur();
		});
		
		$.txtQTY.keyboardToolbar = [btnCancle, flexSpace,flexSpace,flexSpace,btnDone];
		$.txtQTY.keyboardToolbarColor = "#999";
		
	} else {
		$.txtQTY.addEventListener('return', function(e) {
			Ti.API.info('Detected QTY : ' + $.txtQTY.value);
			if($.txtQTY.value != $.txtQTY.orgQty) {
			    	basket.fetch();
					basket.get($.txtQTY.basketId).set({
						qty : parseInt($.txtQTY.value)
					}).save();
					
					basket.fetch();
					console.log("+++++++++++++ After Update QTY +++++++++++");    
					Ti.API.info(basket.toJSON());
			    }
			});
	}
	
	$.txtQTY.keyboardType = Ti.UI.KEYBOARD_NUMBER_PAD;
	
	if(!OS_IOS) {
		$.lblItemTitle.left = 0;
		__.setNormalFontForTablet($.lblItemTitle,16);
		$.lblAttribute.top = 5;
	}
	
	if(Alloy.isTablet) {
		$.rowContainer.width = __.getScreenWidth() - 50;
	}
	
	
	
	var removeIcon = fontIconLoader.getIcon("panacea","trash-o",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgRemove.image = removeIcon;
	$.imgRemove.basketId = basketId;
	attributesString = "";
};

var applyAnimation = function() {
	psAnimation.slowlyAppear($.imgDish);
	psAnimation.slowlyAppear($.itemView);
};

var loadSingleDish = function()
{
	var loaderArgs = {
		callbackFunction : callBackLoadSingleDish,
		url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getItemDetailById + args.item_id + "/shop_id/" + Ti.App.Properties.getString("selected_shop_id")
	};
	loader.get(loaderArgs);
};

var callBackLoadSingleDish = function(respData)
{
	if(respData != null)
	{
		args = respData;
		init();
	}
};

var doRemoveFromBasket = function(e)
{
	basket.fetch();
	basket.get(e.source.basketId).destroy();
	basket.fetch();
	console.log("+++++++++++++ After Delete From Basket +++++++++++");    
	Ti.API.info(basket.toJSON());
	
	var refreshBasket = Alloy.Globals.refreshBasket;
	refreshBasket();
	
	console.log("++++++++ After Removed Item Refresh Basket Item Count at ItemDetail ++++++++++");
	var itemCountRefresh = Alloy.Globals.itemCountRefresh;
	itemCountRefresh();
	
	console.log("++++++++ After Removed Item Refresh Basket Item Count at ItemList ++++++++++");
	var itemCountRefreshItemList = Alloy.Globals.itemCountRefreshItemList;
	itemCountRefreshItemList();
	
	console.log("++++++++ After Removed Item Refresh Basket Item Count at Index ++++++++++");
	var itemCountRefreshIndex = Alloy.Globals.itemCountRefreshIndex;
	var params = {
		selected_shop_id : Ti.App.Properties.getString("selected_shop_id")
	};
	itemCountRefreshIndex(params);
};


if(args.fromWhere == "basket") 
{
	dishImage = args.image;
	where = args.fromWhere;
	qty = args.qty;
	basketId = args.id;
	attributesString = args.basket_item_attribute;
	loadSingleDish();
} else {
	init();
}






