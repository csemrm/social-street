var args = arguments[0] || {};
var psAnimation = require('animation');
var __ = require('platformSupport');
var fontIconLoader = require("icomoonlib");
var dialogBox = require("psdialog");
var loadingWindow = require('loadingWindow');
var loader = require("loader");
var myBasket;
var totalAmount = 0;
var attributeString="";
var basket = Alloy.Collections.basket;
var checkout_shop_id=0;

if (__.isiOS7Plus()) {
	$.AppWrapper.top = 20;
}

var init = function() {
     $.checkoutListView.removeAllChildren();
     loadCheckoutItem();
     loadIcon();
     
     if(args.method != "bank"){
     	$.bankInfoView.opacity = 0;
     	$.bankInfoView.height = 0;
     }
     
     if(!OS_IOS) {
     	$.txtEmail.height = 35;
     	$.txtPhone.height = 35;	
     }
};


var closeWindow = function() {
	psAnimation.out($.checkout);
};

var loadCheckoutItem = function() {
	myBasket =  Alloy.createCollection('basket');
	myBasket.fetch();
	var data = myBasket.where({"shop_id": parseInt(Ti.App.Properties.getString("selected_shop_id"))});
	
	console.log(" Total Records For Checkout : " + data.length);
    
    for(var i=0; i < data.length; i++)
	{
		console.log(' >>>> Basket Item Loop <<<< ');
		var params = {
			id              : data[i].get("id"),
			item_id         : data[i].get("item_id"),
			shop_id         : data[i].get("shop_id"),
			name            : data[i].get("name"),
			description     : data[i].get("description"),
			unit_price      : data[i].get("unit_price"),
			qty             : data[i].get("qty"),
			last_updated_at : data[i].get("last_updated_at"),
			user_id         : data[i].get("user_id"),
			image           : data[i].get("image") || "/images/defaultDish.jpg",
			currency_sign   : data[i].get("currency_sign"),
			serial          : i
		};
		totalAmount += parseInt(data[i].get("qty")) * parseInt(data[i].get("unit_price"));
		oneDish = Alloy.createController('checkoutRow', params).getView();
		$.checkoutListView.add(oneDish);
	} 
	
	$.lblTotal.text = "Total : $" + totalAmount ; 
	$.txtDeliveryAddress.value = Ti.App.Properties.getString('delivery_address');
	$.txtBillingAddress.value = Ti.App.Properties.getString('billing_address');
	$.lblBankAccount.text = "Bank Account : " + Ti.App.Properties.getString('selected_shop_bank_account');
	$.lblBankName.text = "Bank Name : " + Ti.App.Properties.getString('selected_shop_bank_name');
	$.lblBankCode.text = "Bank Code : " + Ti.App.Properties.getString('selected_shop_bank_code');
	$.lblBranchCode.text = "Branch Code : " + Ti.App.Properties.getString('selected_shop_branch_code');
	$.lblSwiftCode.text = "Swift Code : " + Ti.App.Properties.getString('selected_shop_swift_code');
	
	$.txtEmail.value = Ti.App.Properties.getString('email');
};

var applyAnimation = function() {
	psAnimation.slowlyAppear($.orderSummaryView);
	psAnimation.slowlyAppear($.checkoutListView);
	psAnimation.slowlyAppear($.lblTotal);
	psAnimation.slowlyAppear($.deliveryView);
	psAnimation.slowlyAppear($.billingView);
	psAnimation.slowlyAppear($.btnConfirmCheckout);
};

var loadIcon = function() {
	var backIcon = fontIconLoader.getIcon("panacea","back",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgBack.image = backIcon;
	
	var summaryIcon = fontIconLoader.getIcon("panacea","text",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgSummary.image = summaryIcon;
	
	var addressIcon = fontIconLoader.getIcon("panacea","map-marker",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgDeliveryAddress.image = addressIcon;
	$.imgBillingAddress.image = addressIcon;
	
	var emailIcon = fontIconLoader.getIcon("panacea","mail",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgEmail.image = emailIcon;
	
	var phoneIcon = fontIconLoader.getIcon("panacea","phone2",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgPhone.image = phoneIcon;
	
	var bankIcon = fontIconLoader.getIcon("panacea","creditcard",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgBankInfo.image = bankIcon;
	
};

var confirmCheckout = function() {
	if($.txtDeliveryAddress.value == "") {
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.deliveryAddressMissing);
	} else if ($.txtBillingAddress.value == "") {
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.billingAddressMissing);
	} else if ($.txtEmail.value == "") {
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.emailMissing);
	} else if ($.txtPhone.value == "") {
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.phoneMissing);
	} else {
		
		if(args.method == "paypal") {
			var params = {
				amount           : totalAmount,
				delivery_address : $.txtDeliveryAddress.value,
				billing_address  : $.txtBillingAddress.value,
				email            : $.txtEmail.value,
				phone            : $.txtPhone.value
			};
			var contentView = Alloy.createController("paypal",params).getView();
			psAnimation.in(contentView);
		} else if(args.method  == "cod") {
			//COD Flow
			myBasket =  Alloy.createCollection('basket');
			myBasket.fetch();
			var data = myBasket.where({"shop_id": parseInt(Ti.App.Properties.getString("selected_shop_id"))});

			console.log(" Total Records To Send Server : " + data.length);
		    
			var transactions = {};
			var orders = [];
			
		    for(var i=0; i < data.length; i++)
			{
			    checkout_shop_id = data[0].get("shop_id");
			    
			    orders.push({ 
			        "item_id"          		 : data[i].get("item_id"),
			        "shop_id"          		 : data[i].get("shop_id"),
			        "unit_price"       		 : data[i].get("unit_price"),
			        "discount_percent" 		 : data[i].get("discount_percent") || 0,
			        "name"             		 : data[i].get("name"),
			        "qty"              		 : data[i].get("qty"), 
			        "user_id"                : data[i].get("user_id"),
			        "paypal_trans_id" 	     : "",
			        "delivery_address"       : $.txtDeliveryAddress.value,
			        "billing_address"        : $.txtBillingAddress.value,
			        "order_total_amount"     : totalAmount,
			        "basket_item_attribute"  : itemGotAttribute(data[i].get("item_id"),data[i].get("shop_id")),
			    	"payment_method"         : "cod",
			    	"email"                  : $.txtEmail.value,
			    	"phone"                  : $.txtPhone.value
			    });
			    
			    attributeString = "";
			}
		    transactions.orders = orders;
		    console.log(JSON.stringify(transactions));  
		    loadingWindow.startLoading();
		
			var apiArgs = {
				callbackFunction : callBackSuccessOrderSubmit,
				payload          : transactions,
				url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postTransactionsData
			};
			
			loader.post(apiArgs);
			
		} else if (args.method == "bank") {
			//Bank Transfer Flow
			myBasket =  Alloy.createCollection('basket');
			myBasket.fetch();
			var data = myBasket.where({"shop_id": parseInt(Ti.App.Properties.getString("selected_shop_id"))});

			console.log(" Total Records To Send Server : " + data.length);
		    
			var transactions = {};
			var orders = [];
			
		    for(var i=0; i < data.length; i++)
			{
			    checkout_shop_id = data[0].get("shop_id");
			    
			    orders.push({ 
			        "item_id"          		 : data[i].get("item_id"),
			        "shop_id"          		 : data[i].get("shop_id"),
			        "unit_price"       		 : data[i].get("unit_price"),
			        "discount_percent" 		 : data[i].get("discount_percent") || 0,
			        "name"             		 : data[i].get("name"),
			        "qty"              		 : data[i].get("qty"), 
			        "user_id"                : data[i].get("user_id"),
			        "paypal_trans_id" 	     : "",
			        "delivery_address"       : $.txtDeliveryAddress.value,
			        "billing_address"        : $.txtBillingAddress.value,
			        "order_total_amount"     : totalAmount,
			        "basket_item_attribute"  : itemGotAttribute(data[i].get("item_id"),data[i].get("shop_id")),
			    	"payment_method"         : "bank",
			    	"email"                  : $.txtEmail.value,
			    	"phone"                  : $.txtPhone.value
			    });
			    
			    attributeString = "";
			}
		    transactions.orders = orders;
		    console.log(JSON.stringify(transactions));  
		    loadingWindow.startLoading();
		
			var apiArgs = {
				callbackFunction : callBackSuccessOrderSubmit,
				payload          : transactions,
				url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postTransactionsData
			};
			
			loader.post(apiArgs);
		}
		
		
	}
};

var itemGotAttribute = function(item_id,shop_id) {
	var item_attribute =  Alloy.createCollection('item_attribute');
	item_attribute.fetch();
	var data = item_attribute.where({"item_id" : item_id});
	for(var i=0; i < data.length; i++)
	{
		if(data.length-1 == i){
			attributeString += data[i].get("header") + " : " + data[i].get("name");
		} else {
			attributeString += data[i].get("header") + " : " + data[i].get("name") +", ";
		}
	}
	console.log("Before Return attributeString : " + attributeString);
	return attributeString;
};

Alloy.Globals.closeCheckout = function() {
	console.log("Fired >>>>>>>>> closeWindows At checkout.js");
	psAnimation.close($.checkout);
};

var callBackSuccessOrderSubmit = function(respData) {
	loadingWindow.endLoading();
	if(respData != null) {
		if(respData.error == null){
			// Assume that order transactions has been inserted into server DB
			// Need to close 'checkout' page and open success page
			psAnimation.close($.checkout);	
			
			var contentView = Alloy.createController("paymentSuccess",respData).getView();
			psAnimation.in(contentView);
			
			//Clean Basket and Item Attribute
			console.log("+++++ Already Clean Basket ++++++");
			cleanBasket();
			
			console.log("+++++ Already Clean Item Attribute ++++++");
			cleanAttribute();
			
			console.log("++++++++ After Payemnt Suucess, Refresh Basket Item Count at Item Detail ++++++++++");
			var itemCountRefresh = Alloy.Globals.itemCountRefresh;
			itemCountRefresh();
			
			console.log("++++++++ After Payemnt Suucess, Refresh Basket Item Count at Item List ++++++++++");
			var itemCountRefreshItemList = Alloy.Globals.itemCountRefreshItemList;
			itemCountRefreshItemList();
			
			console.log("++++++++ After Payemnt Suucess, Refresh Basket Item Count at Index ++++++++++");
			var itemCountRefreshIndex = Alloy.Globals.itemCountRefreshIndex;
			var params = {
				selected_shop_id : checkout_shop_id
			};
			itemCountRefreshIndex(params);
			
			console.log("+++++ Already Close All Window From Background +++++++++ ");
			var closeCheckout = Alloy.Globals.closeCheckout;
			closeCheckout();
			var closeBasket = Alloy.Globals.closeBasket;
			closeBasket();
			var closeItemDetail = Alloy.Globals.closeItemDetail;
			closeItemDetail();
			var closeItemList = Alloy.Globals.closeItemList;
			closeItemList();
			var closePaymentOption = Alloy.Globals.closePaymentOption;
			closePaymentOption();
		}
	}
};

var cleanBasket = function() {
	//Alloy.Collections.basket.deleteAll();
	Alloy.Collections.basket.deleteRecord({
	  query : {
	     sql : "WHERE shop_id=?",
	     params : checkout_shop_id
	   }
	 });
};

var cleanAttribute = function() {
	Alloy.Collections.item_attribute.deleteRecord({
	  query : {
	     sql : "WHERE shop_id=?",
	     params : checkout_shop_id
	   }
	 });
};

var loadLanguage = function () {
	$.mainTitle.text = Alloy.CFG.Languages.lblCheckout;
	$.lblOrderSummary.text = Alloy.CFG.Languages.lblOrderSummary;
	$.lblTotal.text = Alloy.CFG.Languages.lblTotal;
	$.lblBankInfo.text = Alloy.CFG.Languages.lblBankInfo;
	$.lblBankInfoNote.text = Alloy.CFG.Languages.lblBankInfoNote;
	$.lblEmailAddress.text = Alloy.CFG.Languages.lblEmailAddress;
	$.lblPhoneNo.text = Alloy.CFG.Languages.lblPhoneNo;
	$.lblDeliveryAddress.text = Alloy.CFG.Languages.lblDeliveryAddress;
	$.lblBillingAddress.text = Alloy.CFG.Languages.lblBillingAddress;
	
	$.txtEmail.hintText = Alloy.CFG.Languages.hintTextEmail;
	$.txtPhone.hintText = Alloy.CFG.Languages.hintTextPhoneNo;
	$.txtDeliveryAddress.hintText = Alloy.CFG.Languages.hintTextDeliveryAddress;
	$.txtBillingAddress.hintText = Alloy.CFG.Languages.hintTextBillingAddress;
};

init();   

