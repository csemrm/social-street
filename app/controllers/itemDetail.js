var args = arguments[0] || {};
var __ = require('platformSupport');
var psAnimation = require('animation');
var fontIconLoader = require("icomoonlib");
var loader = require("loader");
var dialogBox = require("psdialog");
var loadingWindow = require('loadingWindow');
var phoneNoPressAction =require("phoneNumberPress");
var finalPrice = 0;
var initialRating = 0;
var basket = Alloy.Collections.basket;
var item_attribute = Alloy.Collections.item_attribute;
var cardRightValue = __.getScreenWidth() - 50;
var picker = require('SimplePicker');
var attributeUI = [];
var att = [];
var att_str = "";
var itemInsideBasket = false;

if (__.isiOS7Plus()) {
	$.AppWrapper.top = 20;
}

var loadReview = function(itemId) {
	var loaderArgs = {
		callbackFunction : initReivew,
		url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getItemDetailById + itemId + "/shop_id/" + Ti.App.Properties.getString("selected_shop_id")
	};
	loader.get(loaderArgs);
};

var openSlider = function() {
	args.fromWhere = "itemDetail";
	var contentView = Alloy.createController("slider", args).getView();
	psAnimation.in(contentView);
};

var closeWindow = function() {
	psAnimation.out($.itemDetail);
};

var doVisitShop = function() {
	var params = {
			name : Alloy.CFG.Languages.appName
		};										  
	var contentView = Alloy.createController("shopContact", params).getView();
	psAnimation.in(contentView);
	
};


var openInquiry = function() {
	loadingWindow.startLoading();
	
	var params = {
		item_id : args.item.id
	};
	var contentView = Alloy.createController("inquiry",params).getView();
	psAnimation.in(contentView);
};

var openReview = function() {
	loadingWindow.startLoading();
	var userId = Ti.App.Properties.getString("userId");
	var params = {
		item_id : args.item.id,
		loadReview : loadReview
	};
	if(userId){	
		var contentView = Alloy.createController("review",params).getView();
		psAnimation.in(contentView);
	}else{
		var contentView = Alloy.createController("userLogin",params).getView();
		psAnimation.in(contentView);	
	}
};

var init = function() {
	var padding = "";
	var screenWidth = __.getScreenWidth();
	var screenHeight = __.getScreenHeight();
	var size = []; 
	size.width = args.item.images[0].width;
	size.height = args.item.images[0].height;
	
	var tmp = __.getGridPhotoSizeCalWidth(size,screenWidth);
	loadingWindow.endLoading();
	
	
	$.imProductImage.image = Alloy.CFG.Urls.imagePathURL + args.item.images[0].path;
	$.imProductImage.width = tmp.width;
	$.imProductImage.height = tmp.height;
	$.productImgView.height = screenHeight / 2.2;
	
	$.lblTitle.text = args.item.name;
	$.lblDescription.text = args.item.description;
	
	$.likeCount.text = args.item.like_count;
	$.reviewCount.text = args.item.review_count;
	$.lblQty.text = Alloy.CFG.Languages.lblQty;
	
	if(args.item.discount_type_id == 0) {
		$.discountView.opacity = 0;
		$.discountView.height = 0;
		$.lblPrice.text =  Alloy.CFG.Languages.lblPrice+Ti.App.Properties.getString("currency_symbol")+" "+ + args.item.unit_price+ " "  + " (" +Ti.App.Properties.getString("currency_short_form")+")";
	} else {
		$.discountView.opacity = 1;
		$.discountView.height = Ti.UI.SIZE;
		finalPrice = args.item.unit_price - (args.item.unit_price * args.item.discount_percent);
		$.lblPrice.text = Alloy.CFG.Languages.lblPriceDiscount+Ti.App.Properties.getString("currency_symbol") + + finalPrice+ " " +  " (" +Ti.App.Properties.getString("currency_short_form")+")";

		$.lblDiscount.text = Alloy.CFG.Languages.lblItemGot + args.item.discount_name;
		
		$.lblMinusAmount.text = Alloy.CFG.Languages.lblMinusAmount + args.item.unit_price * args.item.discount_percent+ " " + Ti.App.Properties.getString("currency_symbol") + " (" +Ti.App.Properties.getString("currency_short_form")+")";
		
	}
	
	for(var i=0; i< args.item.images.length; i++)
	{
		
		if(i==0) {
			var params = {
				imagePath                    : Alloy.CFG.Urls.imagePathURL +  args.item.images[i].path,
				thumbId                      : i,
				width                        : args.item.images[i].width,
				height                       : args.item.images[i].height,
				selected                     : true,
				loadSelctedItemImageFunction : 'loadSlectedItemImage'
			};
		} else {
			var params = {
				imagePath                    : Alloy.CFG.Urls.imagePathURL +  args.item.images[i].path,
				thumbId                      : i,
				width                        : args.item.images[i].width,
				height                       : args.item.images[i].height,
				selected                     : false,
				loadSelctedItemImageFunction : 'loadSlectedItemImage'
			};
		}
		
		contentView = Alloy.createController("thumbImages", params).getView();
		$.thumbImagesScrollView.add(contentView);	
		
	}
	
	if(!OS_IOS) {
		$.thumbImagesScrollView.contentWidth = parseInt(args.item.images.length * __.convertDptoPx(55)) + __.convertDptoPx(30);
		$.lblQty.top = 12;
	} else {
		$.thumbImagesScrollView.contentWidth = parseInt(args.item.images.length * 55) + 30;
		$.txtQTY.height = 25;
	}
	
	$.thumbImagesScrollView.width = __.getScreenWidth() - 70;
	
	if(Alloy.isTablet) {
		$.thumbImagesScrollView.width = 400;
		
		$.thumbContainerView.left = parseInt((parseInt(__.getScreenWidth()) - ($.thumbImagesScrollView.width + 60)))/2;
		padding = 50;
		$.lblTitle.top=10;
		__.setNormalFontForTablet($.lblDescription,16);
		__.setNormalFontForTablet($.lblPrice,16);
	} else {
		padding = 20;
	}
		
	$.midContainer.applyProperties({
		width : screenWidth,
		contentWidth : screenWidth
	});
	
	$.infoContainer.applyProperties({
		left : padding,
		right : padding
	});
	
	
	loadIcon();
	loadLanguage();
	loadReview(args.item.id);
	isLikedChecking();
	isFavouritedChecking();
	increaseTouchCount();
	isRatedChecking();
	checkInsideBasket();
	
	if(args.item.attributes.length > 0) {
		if(args.item.attributes[0].details != "") {
			loadAttributes(args.item.attributes);
		}
	} 
	
};

var applyAnimation = function() {
	psAnimation.slowlyAppear($.imProductImage);
	psAnimation.slowlyAppear($.thumbContainerView);
	
	psAnimation.slowlyAppear($.socialView);
	psAnimation.slowlyAppear($.lblTitle);
	psAnimation.slowlyAppear($.lblDescription);
	
	psAnimation.slowlyAppear($.btnSendInquiry);
	psAnimation.slowlyAppear($.btnAddToCart);
	psAnimation.slowlyAppear($.btnVistShop);
	psAnimation.slowlyAppear($.btnSetAsFavourite);
	
	psAnimation.slowlyAppear($.writeItemReview);
	psAnimation.slowlyAppear($.reviewView);
	psAnimation.slowlyAppear($.shareView);
};


Ti.App.addEventListener('loadSlectedItemImage',function() {
	var index = Ti.App.Properties.getString("selectedThumbImageIndex");
	var screenWidth = __.getScreenWidth();
	var screenHeight = __.getScreenHeight();
	var size = []; 
	
	try{
		size.width = args.item.images[index].width;
		size.height = args.item.images[index].height;
	}catch(E){}
	
	var tmp = __.getGridPhotoSizeCalWidth(size,screenWidth);
	
	try{
		$.imProductImage.image = Alloy.CFG.Urls.imagePathURL + args.item.images[index].path;
		size.width = args.item.images[index].width;
		size.height = args.item.images[index].height;
	}catch(E){}
	
	$.imProductImage.width = tmp.width;
	$.imProductImage.height = tmp.height;

	
	$.productImgView.height = screenHeight / 2.2;
	
	psAnimation.slowlyAppear($.imProductImage);
	
	$.thumbImagesScrollView.removeAllChildren();
	for(var i=0; i< args.item.images.length; i++) 
	{
		
		if(i == index){
			var params = {
				imagePath                    : Alloy.CFG.Urls.imagePathURL +  args.item.images[i].path,
				thumbId                      : i,
				width                        : args.item.images[i].width,
				height                       : args.item.images[i].height,
				selected                     : true,
				loadSelctedItemImageFunction : 'loadSlectedItemImage'
			};
		} else {
			var params = {
				imagePath                    : Alloy.CFG.Urls.imagePathURL +  args.item.images[i].path,
				thumbId                      : i,
				width                        : args.item.images[i].width,
				height                       : args.item.images[i].height,
				selected                     : false,
				loadSelctedItemImageFunction : 'loadSlectedItemImage'
			};
		}
		
		contentView = Alloy.createController("thumbImages", params).getView();
		$.thumbImagesScrollView.add(contentView);	
	}

});

var gotoRightMost = function() {
	$.thumbImagesScrollView.scrollToBottom();
};

var gotoLeftMost = function() {
	$.thumbImagesScrollView.scrollTo(0,0);
};

var initReivew = function(data) {
	if(data.reviews){
		var reviews = data.reviews;
		var length = reviews.length;
		var review = null;
		
		$.reviewView.removeAllChildren();
		$.reviewCount.text = length;
		
		if(args.item.reviewCount) {
			args.item.reviewCount.text = length;
		}
		
		for(var i=0; i<length; i++) {
			review = Alloy.createController('reviewRow', reviews[i]).getView();
			$.reviewView.add(review);		
		}
	}
	
};

var doLike = function() {
	if(Titanium.Network.online == true) {
		var uid = Ti.App.Properties.getString("userId");
		if(!Ti.App.Properties.getString("userId")){
		 	var params = {
				item_id : 0,
			};
		 	var contentView = Alloy.createController("userLogin",params).getView();
			psAnimation.in(contentView);
		 	dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.needForLoginMesssage);
		} else {                                 
			var payloadJSON = {"appuser_id": Ti.App.Properties.getString("userId"),"shop_id" : Ti.App.Properties.getString("selected_shop_id")};
			var apiArgs = {
				callbackFunction : callBackDoLike,
				payload : payloadJSON,
				url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postLikeData + args.item.id
			};
			loader.post(apiArgs);
		}	
				
	}else{
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.offlineMessage);
	}
};

var callBackDoLike = function(data) {
	$.imgLike.animate({
		width : 45,
		height : 45,
		duration : 100
	}, function(){
		$.imgLike.animate({
			width : 25,
			height : 25,
			duration : 100
		}, function(){
			
			$.imgLike.width = 20;
			$.imgLike.height = 20;
			
			myicon = fontIconLoader.getIcon("panacea","thumbs-up",35,{color:Alloy.CFG.Colors.ItemIconColor_Dark});
			$.imgLike.image = myicon;
			
			if(data.success){
				$.likeCount.text = data.total;
				if(args.item.likeCount){
					args.item.likeCount.text = data.total;
				}
			}else{
				dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.alreadyLikeMessage);
			}
		});
	});
	Ti.API.info(JSON.stringify(data));
};


var doFavourite = function() {
	if(Titanium.Network.online == true) {
		var uid = Ti.App.Properties.getString("userId");
		
		if(!Ti.App.Properties.getString("userId")) {
		 	
		 	var params = {
				item_id : 0,
			};
		 	
		 	var contentView = Alloy.createController("userLogin",params).getView();
			psAnimation.in(contentView);	
			dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.needForFavouriteMesssage);
		} else {
			var payloadJSON = {"appuser_id": Ti.App.Properties.getString("userId"), "shop_id" : Ti.App.Properties.getString("selected_shop_id")};
			var apiArgs = {
				callbackFunction : callBackDoFavourite,
				payload : payloadJSON,
				url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postFavouriteData + args.item.id
			};
			loader.post(apiArgs);
		}
		
	} else {
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.offlineMessage);
	}
};

var callBackDoFavourite = function(data) {
	if(!data.success) {
			dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.alreadyFavouriteMessage);
	} else {
		$.btnSetAsFavourite.backgroundColor = Alloy.CFG.Colors.DisableColor;
		$.btnSetAsFavourite.title = Alloy.CFG.Languages.alreadyFavouritedItem;
		$.btnSetAsFavourite.color = "#000000";
	}
};

var loadIcon = function() {
	var likeIcon = fontIconLoader.getIcon("panacea","thumbs-up",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgLike.image = likeIcon;
	
	var commentIcon = fontIconLoader.getIcon("panacea","comment",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgReview.image = commentIcon;
	
	var priceIcon = fontIconLoader.getIcon("panacea","tag2",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgPrice.image = priceIcon;
	
	var discountIcon = fontIconLoader.getIcon("panacea","gift",35,{color:Alloy.CFG.Colors.DiscountColor});
	$.imgDiscount.image = discountIcon;
	
	var reviewIcon = fontIconLoader.getIcon("panacea","pencil-square",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgWriteReviewIcon.image = reviewIcon;
	
	var backIcon = fontIconLoader.getIcon("panacea","back",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgBack.image = backIcon;
	
	var facebookIcon = fontIconLoader.getIcon("panacea","facebook-square",35,{color:Alloy.CFG.Colors.facebookColor});
	$.imgFacebook.image = facebookIcon;
	
	var twitterIcon = fontIconLoader.getIcon("panacea","twitter-square",35,{color:Alloy.CFG.Colors.twitterColor});
	$.imgTwitter.image = twitterIcon;
	
	var cartIcon = fontIconLoader.getIcon("panacea","shopping-cart",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgCart.image = cartIcon;
	
	var qtyIcon = fontIconLoader.getIcon("panacea","bag2",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgQty.image = qtyIcon;
	
	var bagIcon = fontIconLoader.getIcon("panacea","bag2",35,{color:Alloy.CFG.Colors.CartColor});
	$.imgBag.image = bagIcon;
	
};

var loadLanguage = function() {
	$.btnSendInquiry.title = Alloy.CFG.Languages.btnSendInquiry;
	$.btnAddToCart.title = Alloy.CFG.Languages.btnAddToCart;
	$.btnSetAsFavourite.title = Alloy.CFG.Languages.btnSetAsFavourite;
	$.btnVistShop.title = Alloy.CFG.Languages.lblVisitShop;
	$.lblWriteReview.text = Alloy.CFG.Languages.lblWriteReview;
	$.mainTitle.text = Alloy.CFG.Languages.lblDetailInformation;
};

var isLikedChecking = function() {
	if(Titanium.Network.online == true) {
	
		var payloadJSON = {"appuser_id": Ti.App.Properties.getString("userId"), "shop_id" :Ti.App.Properties.getString("selected_shop_id")};
		
		var apiArgs = {
			callbackFunction : callBackIsLikedChecking,
			payload          : payloadJSON,
			url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postIsLiked + args.item.id
		};
		loader.post(apiArgs);
	}
};


var callBackIsLikedChecking = function(feeds) {
	if(feeds.status == "yes") {
		myicon = fontIconLoader.getIcon("panacea","thumbs-up",35,{color:Alloy.CFG.Colors.ItemIconColor_Dark});
		$.imgLike.image = myicon;
		$.likeCount.text = feeds.total;
	}
};

var isUnLikedChecking = function() {
	if(Titanium.Network.online == true) {
	
		var payloadJSON = {"appuser_id": Ti.App.Properties.getString("userId")};
		
		var apiArgs = {
			callbackFunction : callBackIsUnLikedChecking,
			payload          : payloadJSON,
			url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postIsUnLiked + args.item.id
		};
		loader.post(apiArgs);
	}
};

var callBackIsUnLikedChecking = function(feeds) {
	if(feeds.status == "yes") {
		myicon = fontIconLoader.getIcon("panacea","thumbs-down",35,{color:Alloy.CFG.Colors.ItemIconColor_Dark});
		$.imgUnLike.image = myicon;
	}
};

var isFavouritedChecking = function() {
	if(Titanium.Network.online == true) {
		if(Ti.App.Properties.getString("userId")){
			var payloadJSON = {"appuser_id": Ti.App.Properties.getString("userId"), "shop_id" : Ti.App.Properties.getString("selected_shop_id")};
		//	alert(Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postIsFavourited + args.item.id);
			var apiArgs = {
				callbackFunction : callBackIsFavouritedChecking,
				payload          : payloadJSON,
				url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postIsFavourited + args.item.id
			};
			console.log(payloadJSON);
			loader.post(apiArgs);
		}
	}
};

var callBackIsFavouritedChecking = function(feeds) {
	if(feeds.status == "yes") {
		$.btnSetAsFavourite.backgroundColor = Alloy.CFG.Colors.DisableColor;
		$.btnSetAsFavourite.title = Alloy.CFG.Languages.alreadyFavouritedItem;
		$.btnSetAsFavourite.color = "#000000";
	}
};

var increaseTouchCount = function() {
	if(Ti.App.Properties.getString("userId") != null ){
		var payloadJSON = {"appuser_id": Ti.App.Properties.getString("userId"), 'shop_id' : Ti.App.Properties.getString("selected_shop_id")};
	} else {
		var payloadJSON = {"appuser_id": 0, 'shop_id' : Ti.App.Properties.getString("selected_shop_id")};
	}
	
	var loaderArgs = {
		callbackFunction : callBackIncreaseTouchCount,
		url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postTouchData + args.item.id,
		payload : payloadJSON
	};
	loader.post(loaderArgs);
};

var callBackIncreaseTouchCount = function(feeds) {
	if(feeds.success) {
		console.log("Touch Count Successfully Increased.");
	} else {
		console.log("Gor Error when insert touch count.");
	}
};

var doFacebookShare = function() {
	if(OS_IOS) {
		var Social = require('dk.napp.social');
		if(Social.isFacebookSupported()) { 
			Social.facebook({
				text  : Alloy.CFG.Languages.facebookShareMessage,
				image : Alloy.CFG.Languages.facebookShareImageURL,
				url   : Alloy.CFG.Languages.facebookShareAppURL
			});
		}
	}else{ 
		var facebookShareAndroid = require('facebook');
		
		facebookShareAndroid.appid = eval(Alloy.CFG.SocialKeys.facebookAppKey);
		facebookShareAndroid.permissions = ['publish_stream'];
		facebookShareAndroid.authorize();
		
		var data = {
		    link        : Alloy.CFG.Languages.facebookShareAppURL,
		    name        :  Alloy.CFG.Languages.facebookShareTitle,
		    message     : Alloy.CFG.Languages.facebookShareMessage,
		    caption     : Alloy.CFG.Languages.facebookShareTitle,
		    picture     : Alloy.CFG.Languages.facebookShareImageURL,
		    description : Alloy.CFG.Languages.facebookShareMessage
		};
		    facebookShareAndroid.dialog("feed", data, function(e) {
		        if (e.success && e.result) {
		            dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.facebookShareSuccess);
		        } else {
		 
		        }
		    });
	}
};


var doTwitterShare = function() {
	if(OS_IOS) {
		var Social = require('dk.napp.social');
		if(Social.isTwitterSupported()){ 
			Social.twitter({
				text  : Alloy.CFG.Languages.twitterShareMessage,
				image : Alloy.CFG.Languages.twitterShareImageURL,
				url   : Alloy.CFG.Languages.twitterShareAppURL
			});
		} 
	} else { 
		var twitterSupport = require("twitterSupport");
		var message = Alloy.CFG.Languages.twitterShareMessage;
		var consumerKey = Alloy.CFG.SocialKeys.twitterConsumerKey;
		var consumerSecret =  Alloy.CFG.SocialKeys.twitterConsumerSecret;
		twitterSupport.tweet(message, consumerKey, consumerSecret);
	}
	
};

var isRatedChecking = function() {
	if(Titanium.Network.online == true) {
	
		var payloadJSON = {"shop_id" : Ti.App.Properties.getString("selected_shop_id")};
		
		var apiArgs = {
			callbackFunction : callBackIsRatedChecking,
			payload          : payloadJSON,
			url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postIsRatedData + args.item.id
		};
		loader.post(apiArgs);
	}
};

var callBackIsRatedChecking = function(respData) {
	if(respData.status == "yes") {
		initialRating = respData.total;
		$.starwidget.setRating(respData.total);
	} else {
		$.starwidget.setRating(0);
	}
};

var doRating = function(rating) {
	if(Titanium.Network.online == true) {
		
		if(Ti.App.Properties.getString("userId")) {
	
			var payloadJSON = {"appuser_id": Ti.App.Properties.getString("userId"), "shop_id" : Ti.App.Properties.getString("selected_shop_id"), "rating" : rating};
			
			var apiArgs = {
				callbackFunction : callBackDoRating,
				payload          : payloadJSON,
				url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postRatingData + args.item.id
			};
			console.log(JSON.stringify(payloadJSON));
			loader.post(apiArgs);
			
		} else {
			$.starwidget.setRating(initialRating);
			
			var params = {
				item_id : 0,
			};
		 	var contentView = Alloy.createController("userLogin",params).getView();
			psAnimation.in(contentView);
			
			dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.needForRatingMessage);
		}
		
	} else {
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.offlineMessage);
	}
};

var callBackDoRating = function(respData) {
	if(respData.success) {
		$.starwidget.setRating(respData.rating);
		initialRating = respData.rating;
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.ratingSuccessMessage);
	} else if(respData.error) {
		$.starwidget.setRating(initialRating);
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.alreadyRateMessage);
	}
};

var openBasket = function() {
	var myModel =  Alloy.createCollection('basket');
	myModel.fetch();
	//myModel.fetch({query:"SELECT * FROM basket"});
	var data = myModel.where({"shop_id": parseInt(Ti.App.Properties.getString("selected_shop_id"))});
	
	if(data.length >= 1){
		var closeItemList = Alloy.Globals.closeItemList;
		closeItemList();
		
		var contentView = Alloy.createController("basket").getView();
		psAnimation.in(contentView);
	} else {
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.noItemsInsideBasket);
	}
};

var doAddToCart = function(e) {
	var userId = Ti.App.Properties.getString("userId");
	
	if(userId) {	
		// User can add only one time for One Item
		// Need to filter with Item and User ID
		var myModelChecking =  Alloy.createCollection('basket');
		myModelChecking.fetch({query:"SELECT * FROM basket where item_id='"+ args.item.id +"' and user_id='"+userId+"'"});
		
		if(myModelChecking.length < 1) { 
			// Save into local basket model
			
			if(args.item.discount_type_id == 0) {
			
				var basketModel = Alloy.createModel('basket',{
					item_id          : args.item.id,
					shop_id          : Ti.App.Properties.getString("selected_shop_id"),
					name             : args.item.name,
					description      : args.item.description,
					unit_price       : args.item.unit_price,
					discount_percent : args.item.discount_percent || 0,
					qty              : $.txtQTY.value,
					user_id          : Ti.App.Properties.getString("userId"),
					image            : args.item.images[0].path,
					currency_sign    : Ti.App.Properties.getString("currency_symbol"),
					added            : __.toMysqlDateFormat()
				});
			
			} else {
				
				var basketModel = Alloy.createModel('basket',{
					item_id          : args.item.id,
					shop_id          : Ti.App.Properties.getString("selected_shop_id"),
					name             : args.item.name,
					description      : args.item.description,
					unit_price       : args.item.unit_price - (args.item.unit_price * args.item.discount_percent),
					discount_percent : args.item.discount_percent || 0,
					qty              : $.txtQTY.value,
					user_id          : Ti.App.Properties.getString("userId"),
					image            : args.item.images[0].path,
					currency_sign    : Ti.App.Properties.getString("currency_symbol"),
					added            : __.toMysqlDateFormat()
				});
			}
			
			basket.add(basketModel);
			basketModel.save();
			basket.fetch();
			Ti.API.info(basket.toJSON());
			
			$.txtQTY.blur();
			
			//Save Item Attribute 
			saveAttribute();
			
			//Need to show total item count in basket
			var myModel =  Alloy.createCollection('basket');
			myModel.fetch();
			var data = myModel.where({"shop_id": parseInt(Ti.App.Properties.getString("selected_shop_id"))});
	
			//myModel.fetch({query:"SELECT * FROM basket"});
			console.log(" Total Records in Basket : " + data.length);
			
			startAddToCartAnimation(e, data.length);
			
			console.log("++++++++ After added Item Refresh Basket Item Count at ItemDetail ++++++++++");
			var itemCountRefresh = Alloy.Globals.itemCountRefresh;
			itemCountRefresh();
			
			console.log("++++++++ After added Item Refresh Basket Item Count at ItemList ++++++++++");
			var itemCountRefreshItemList = Alloy.Globals.itemCountRefreshItemList;
			itemCountRefreshItemList();
			
			console.log("++++++++ After added Item Refresh Basket Item Count at Index ++++++++++");
			var itemCountRefreshIndex = Alloy.Globals.itemCountRefreshIndex;
			var params = {
				selected_shop_id : Ti.App.Properties.getString("selected_shop_id")
			};
			itemCountRefreshIndex(params);
			
		} else {
			
			Alloy.Collections.basket.updateRecord({
			  query : {
			     columns    : ["qty"],
			     values     : [$.txtQTY.value],
			     whereKey   : ["item_id", "user_id"],
			     whereValue : [args.item.id, userId]
			   }
			 });
			 Alloy.Collections.basket.fetch();
			 Ti.API.info(Alloy.Collections.basket.toJSON());
			 
			 dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.updateCartSuccessMessage);
			 
		}
		
	} else {
		//user need to login first
		loadingWindow.startLoading();
		var params = {
			item_id : 0
		};
		var contentView = Alloy.createController("userLogin",params).getView();
		psAnimation.in(contentView);
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.needForAddToCartMessage);
	}
	
};

var startAddToCartAnimation = function(e,countValue) {
	var localPoint = {x:e.x, y:e.y};
	var convPoint = e.source.convertPointToView(localPoint, $.itemDetail);
	
	if(!OS_IOS){
		convPoint.x = __.convertPxtoDp(convPoint.x);
		convPoint.y = __.convertPxtoDp(convPoint.y);
	}
	
	$.imgBag.animate({
		opacity : 2,
		left : convPoint.x - 20,
		top : convPoint.y - 20,
		duration : 1
	},function(){
		$.imgBag.animate({
			opacity : 8,
			left : cardRightValue,
			top : 30,
			duration : 800
		}, function(){
			$.imgBag.opacity = 0;
			
			$.imgCart.animate({
				width : 40,
				height : 40,
				duration : 100
			}, function(){
				$.imgCart.animate({
					width : 30,
					height : 30,
					duration : 100
				}, function(){
					$.btnCount.setTitle(countValue+"");
					checkInsideBasket();
				});
			});
		});
	});
	
};

var loadItemCount = function() {
	var myModel =  Alloy.createCollection('basket');
	myModel.fetch();
	//myModel.fetch({query:"SELECT * FROM basket"});
	var data = myModel.where({"shop_id": parseInt(Ti.App.Properties.getString("selected_shop_id"))});
	
	var itemCount = data.length;
	
	if(itemCount >= 1){
		$.btnCount.title = itemCount.toString();
		$.btnCount.opacity = 1;
	} else {
		$.btnCount.opacity = 0;
	}
};

var checkInsideBasket = function() {
	var myModel =  Alloy.createCollection('basket');
	myModel.fetch({query:"SELECT * FROM basket where item_id='"+ args.item.id +"'"});
	if(myModel.length >= 1){
		$.btnAddToCart.title = Alloy.CFG.Languages.lblUpdateToCart;
		$.btnAddToCart.backgroundColor = Alloy.CFG.Colors.DisableColor;
		$.btnCount.opacity = 1;
		$.txtQTY.value = myModel.at(0).get("qty");
		itemInsideBasket = true;
	}
};

Alloy.Globals.itemCountRefresh = function() {
	console.log("Fired >>>>>>>>> Item Count Refresh At itemDetail.js");
	loadItemCount();
};

Alloy.Globals.closeItemDetail = function() {
	closeWindow();
};

$.itemDetail.addEventListener('open', function() {
	loadingWindow.endLoading();
	init();
	$.starwidget.init(doRating); 
	loadItemCount();
	
	if(!OS_IOS) {
		$.txtQTY.blur();
	}
});

var loadAttributes = function(attributes) {
	
	console.log(attributes);
	var defaultIndex = 0;
	for(var i=0; i< attributes.length; i++)
	{
		
		attributeUI.lblAttributeHeader = Ti.UI.createLabel({
			text : Alloy.CFG.Languages.lblChoose + attributes[i].name,
			left : 0,
			font :{ fontFamily: "Monda-Regular" },
			top : 2,
			color : Alloy.CFG.Colors.IconColor,
			headerValue : attributes[i].name
		});
		$.attributeView.add(attributeUI.lblAttributeHeader);
		
		if(itemInsideBasket) {
			console.log(">>>>> Inside - Yes <<<<");
			attributeUI.attributePicker = picker.SimplePicker({
				header        : attributes[i].name,
				values        : eval(attributes[i].detailString),
				position      : i, //header group id
				selectedValue : getAttribute(attributes[i].name,"name"),
				selectedIndex : getAttribute(attributes[i].name,"index")
			});
		} else {
			console.log(">>>>> Inside - No <<<<");
			attributeUI.attributePicker = picker.SimplePicker({
				header : attributes[i].name,
				values : eval(attributes[i].detailString),
				position : i //header group id
			});
		}
		
		
		$.addClass(attributeUI.attributePicker, "bottom2 span12");
		$.attributeView.add(attributeUI.attributePicker);
		
		att[i] = attributes[i].name + "#" + attributes[i].details[0].name + "#" + defaultIndex;
		
		attributeUI.attributePicker.addEventListener('TUchange', function(e) {
			console.log(e.header + " : " + e.value + " >> " + e.position + "   >>>  "+ e.attributeIndex);
			console.log(">>>>>>> Update Attribute <<<<<<<<<<< ");
			var myModel =  Alloy.createCollection('item_attribute');
			myModel.fetch({query:"SELECT * FROM item_attribute where item_id='"+ args.item.id +"'"});
			if(myModel.length >= 1){
				 console.log(">>>> Update Process Success <<<<");
				 
				 Alloy.Collections.item_attribute.updateRecord({
				  query : {
				     columns    : ["name","attributeIndex"],
				     values     : [e.value,e.attributeIndex],
				     whereKey   : ["item_id", "shop_id", "header"],
				     whereValue : [args.item.id, Ti.App.Properties.getString("selected_shop_id"),e.header]
				   }
				 });
				 Alloy.Collections.item_attribute.fetch();
				 Ti.API.info(Alloy.Collections.item_attribute.toJSON());
			} else {
				for(var j=0; j<att.length;j++) {
					if(e.position == j){//header 
						att[e.position] = e.header + "#" +e.value +"#"+ e.attributeIndex;
					}
				}
				
				 console.log(">>>> Not yet inside item_attribute table so just add into temp array <<<< " +  e.attributeIndex);
			}
			
			
		});
		
		
	}
};

var saveAttribute = function() {
	if(att.length > 0){
		console.log(">>>>>>>> SaveAttribute <<<<<<<<<<");
		for(var k=0;k<att.length;k++){
			var att_array = att[k].split('#');
			
			var attributeModel = Alloy.createModel('item_attribute',{
				item_id          : args.item.id,
				shop_id          : Ti.App.Properties.getString("selected_shop_id"),
				name             : att_array[1].trim(),
				header           : att_array[0].trim(),
				attributeIndex   : att_array[2]
			});
			
			item_attribute.add(attributeModel);
			attributeModel.save();
		}
		item_attribute.fetch();
		Ti.API.info(item_attribute.toJSON());	
		
	}
};

var getAttribute = function(header_type,option) {
	item_attribute =  Alloy.createCollection('item_attribute');
	item_attribute.fetch({query:"SELECT * FROM item_attribute where item_id='"+ args.item.id +"' and shop_id='"+Ti.App.Properties.getString("selected_shop_id")+"'"});
	
	for(var i=0; i < item_attribute.length; i++)
	{
		if(item_attribute.at(i).get("header") == header_type) {
			if(option == "name") {
				console.log("name >>> " + item_attribute.at(i).get("name"));
				return item_attribute.at(i).get("name");
			} else if (option == "index"){
				console.log("index >>> " + item_attribute.at(i).get("attributeIndex"));
				return item_attribute.at(i).get("attributeIndex");
			}
		} 
	}
};



