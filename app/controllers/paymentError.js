var args = arguments[0] || {};
var fontIconLoader = require("icomoonlib");
var psAnimation = require('animation');
var __ = require('platformSupport');
var loadingWindow = require('loadingWindow');
var dialogBox = require("psdialog");

if (__.isiOS7Plus()) {
	$.AppWrapper.top = 20;
}

var init = function() {
	loadingWindow.endLoading();
	loadIcon();
	$.lblErrorMessage.text = Alloy.CFG.Languages.lblErrorMessage;
	$.mainTitle.text = Alloy.CFG.Languages.lblPaymentError;
	applyAnimation();
};

var applyAnimation = function() {
	psAnimation.slowlyAppear($.errorViewContainer);
};

var closeWindow = function() {
	psAnimation.out($.paymentError);
};

var gotoBasket = function() {
	psAnimation.close($.paymentCancel);
	
	console.log("+++++ Already Close All Window From Background [Payment Cancel] +++++++++ ");
	var closeWindows = Alloy.Globals.closeWindows;
	closeWindows();
	
	var contentView = Alloy.createController("basket").getView();
	psAnimation.in(contentView);
};

var clearBasket = function() {
	//Alloy.Collections.basket.deleteAll();
	console.log("Delete Basket items by shop id : " + args.checkout_shop_id);
	Alloy.Collections.basket.deleteRecord({
	  query : {
	     sql : "WHERE shop_id=?",
	     params : args.checkout_shop_id
	   }
	});
	
	console.log("Delete Attribute by shop id : " + args.checkout_shop_id);
	Alloy.Collections.item_attribute.deleteRecord({
	  query : {
	     sql : "WHERE shop_id=?",
	     params : args.checkout_shop_id
	   }
	}); 
	
	console.log("++++++++ Refresh Basket Item Count [Payment Cancel] ++++++++++");
	var itemCountRefresh = Alloy.Globals.itemCountRefresh;
	itemCountRefresh();
	dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.cleanSuccessBasket);
};

var doClose = function() {
	closeWindow();
};

var loadIcon = function() {
	var backIcon = fontIconLoader.getIcon("panacea","back",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgBack.image = backIcon;
	
	var errorIcon = fontIconLoader.getIcon("panacea","warning2",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgError.image = errorIcon;
};

init();
