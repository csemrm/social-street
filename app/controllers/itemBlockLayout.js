var args = arguments[0] || {};
var fontIconLoader = require("icomoonlib");
var __ = require('platformSupport');
var psAnimation = require('animation');
var loadingWindow = require('loadingWindow');
var scale = __.getGridPhotoSizeCalWidth(args.scale, args.viewWidth);

var init = function() {
	$.itemImg.image = args.image;
	$.itemImg.width = scale.width;
	$.itemImg.height = scale.height;
	console.log("scale height " + scale.height);
	
	$.itemBlockLayout.iHeight = scale.height + 80;
	$.itemBlockLayout.iWidth = scale.width;
	
	$.itemImgView.width = scale.width;
	$.itemImgView.height = scale.height;
	$.itemName.text = args.title;
	$.itemPrice.text = "Price : " + Ti.App.Properties.getString("currency_symbol")+ args.price + " "  + " (" +Ti.App.Properties.getString("currency_short_form")+")";
	
	$.likeCount.text = args.item.like_count;
	$.reviewCount.text = args.item.review_count;
	
	args.item.likeCount = $.likeCount;
	args.item.reviewCount= $.reviewCount;
	
	$.rowViewContainer.width =  args.viewWidth - 10; 
	
	loadIcon();
	//applyAnimation();
};


var applyAnimation = function() {
	psAnimation.slowlyAppear($.itemImg);
	psAnimation.slowlyAppear($.itemName);
	psAnimation.slowlyAppear($.itemPrice);
	psAnimation.slowlyAppear($.socialView);
};

var openProductDetail = function(e) {
	loadingWindow.startLoading();
	var contentView = Alloy.createController("itemDetail", args).getView();
	psAnimation.in(contentView);
};

var loadIcon = function() {
	var likeIcon = fontIconLoader.getIcon("panacea","thumbs-up",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgLike.image = likeIcon;
	
	var commentIcon = fontIconLoader.getIcon("panacea","comment",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgReview.image = commentIcon;
	
	//var unlikeIcon = fontIconLoader.getIcon("panacea","thumbs-down",35,{color:Alloy.CFG.Colors.IconColor});
	//$.imgUnLike.image = unlikeIcon;
};

init();




