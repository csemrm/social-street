var __ = require('platformSupport');
var myAnimation = require('animation');
var loadingWindow = require("loadingWindow");
var loader = require("loader");
var args = arguments[0] || {};
var ITEM_WIDTH = 160;
var NUM_OF_COL = 0;
var SCREEN_WIDTH = 0;
var SCREEN_HEIGHT = 0;
var EXTRA_PADDING = 0;
var ITEMS_HEIGHT = [];
var ACTUAL_TOTAL_WIDTH = 0;
var loader = require("loader");
var fontIconLoader = require("icomoonlib");
var top = 50;
var psScrollView = require('psScrollView'); 
var contentView;
var _psScrollView = new psScrollView();
var psAnimation = require('animation');

var selectCategory = function(e) {
	//Ti.App.fireEvent('shakeSubCategoryTitle');
};

var applyAnimation = function() {
	psAnimation.slowlyAppear($.lblSearchKeyword);
	psAnimation.slowlyAppear($.imCategoryImage);
	psAnimation.slowlyAppear($.txtSearch);
	psAnimation.slowlyAppear($.lblAvailable);
};

var init = function() {
	$.imCategoryImage.image = args.subCatImage;
	$.lblAvailable.text = Alloy.CFG.Languages.lblAvailableItems + args.subCatName;
	$.lblSearchKeyword.text = Alloy.CFG.Languages.lblKeywordSearch;
	$.txtSearch.hintText = Alloy.CFG.Languages.hintTextKeywordSearch;
	
	var limit = Alloy.CFG.LimitOfPosts;
	Ti.App.Properties.setInt("From", 0);
	
	var loaderArgs = {
		callbackFunction : callBackLoadProducts,
		url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getItemsByPagination +  Ti.App.Properties.getString("selected_shop_id") +"/sub_cat_id/" + args.subCatId + "/item/all/count/"+limit+"/from/0"
	};
	console.log(Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getItemsByPagination +  Ti.App.Properties.getString("selected_shop_id") +"/sub_cat_id/" + args.subCatId + "/item/all/count/"+limit+"/from/0");
	loader.get(loaderArgs);
	
	
	loadLanguage();
	applyAnimation();
	
	if(!OS_IOS) {
		console.log(">>>>>>>>>>>>>>>>>> hide keyboard <<<<<<<<<<<<<<<");
		Ti.UI.Android.hideSoftKeyboard();
		$.txtSearch.blur();
	}
};

var callBackLoadProducts = function(respData) {
	args.items = respData;
	
	console.log("Sub Cat Length " + args.items.length + "---->>> sub cat id : " + args.items.sub_cat_id);
	
	contentView = Ti.UI.createView({
			width:Ti.UI.FILL,
			height: Ti.UI.FILL,
			top:0
	});
	contentView.add(_psScrollView);
	$.categoryView.add(contentView);
	
	if(Alloy.isTablet) {
		ITEM_WIDTH = 200;
	}
	
	SCREEN_WIDTH = __.getScreenWidth();
	SCREEN_HEIGHT = __.getScreenHeight();
	calculateCol();
	
	for(var i=0; i<args.items.length; i++) {
		var scale = {
				width : args.items[i].images[0].width,
				height :  args.items[i].images[0].height
		};
		var params = {
			image     : Alloy.CFG.Urls.imagePathURL + args.items[i].images[0].path,
			title     : args.items[i].name,
			price     : args.items[i].unit_price,
			like      : i * 21,
			msg       : i * 13,
			scale     : scale,
			viewWidth : ITEM_WIDTH,
			item      : args.items[i]
		};
		
		addView(itemGridLayout(params));
		
	}
	
	
	Ti.App.Properties.setInt("From", 5);
	
};

var calculateCol = function() {
	
	NUM_OF_COL = parseInt(SCREEN_WIDTH / ITEM_WIDTH);	
	EXTRA_PADDING = SCREEN_WIDTH - (NUM_OF_COL * ITEM_WIDTH);
	
	ITEM_WIDTH += (EXTRA_PADDING / NUM_OF_COL);
	EXTRA_PADDING %= NUM_OF_COL;
	
	if(NUM_OF_COL != null) {
		for(var i=0; i<NUM_OF_COL; i++)
		{
			ITEMS_HEIGHT.push(0);
		}
	}
	ACTUAL_TOTAL_WIDTH = SCREEN_WIDTH - EXTRA_PADDING;	
};

var getColNum = function() {
	var col = 0;
	
	for(var i=1; i<NUM_OF_COL; i++)
	{
		if(ITEMS_HEIGHT[col] > ITEMS_HEIGHT[i]) {
			col = i;
		}
	}
	return col;
}; 

var itemGridLayout = function(params) {
	var layout = Alloy.createController('itemBlockLayout', params).getView();
	return layout;
};

var addView = function(view) {
	var col = getColNum();
	var leftPadding = (col) * ITEM_WIDTH;
	
	view.left = leftPadding;
	view.top = ITEMS_HEIGHT[col];
	ITEMS_HEIGHT[col] += view.iHeight + 20;
	_psScrollView.addView(view);
};

var updateFromVal = function() {
	var limit = Alloy.CFG.LimitOfPosts;
	var from = Ti.App.Properties.getInt("From");
	Ti.App.Properties.setInt("From", (limit+from));
};

var callBackInfiniteScrolling = function(data) {
	
	for(var i=0; i<data.length; i++){
		var scale = {
				width : data[i].images[0].width,
				height :  data[i].images[0].height
		};
		var params = {
			image     : Alloy.CFG.Urls.imagePathURL + data[i].images[0].path,
			title     : data[i].name,
			price     : data[i].unit_price,
			like      : i * 21,
			msg       : i * 13,
			scale     : scale,
			viewWidth : ITEM_WIDTH,
			item      : data[i]
		};
		
		addView(itemGridLayout(params));
	}
	 updateFromVal();
	_psScrollView.infiniteScrollingCompleted();	
	
};

_psScrollView.addEventListener("InfiniteScrolling", function(e) {
	Ti.API.info("InfiniteScrolling event ----------> start loading data");
	var limit = Alloy.CFG.LimitOfPosts;
	var from = Ti.App.Properties.getInt("From");
	
	if($.txtSearch.value == "") {
		var loaderArgs = {
			callbackFunction : callBackInfiniteScrolling,
			url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getItemsByPagination + Ti.App.Properties.getString("selected_shop_id") +"/sub_cat_id/" + args.subCatId + "/item/all/count/"+limit+"/from/" + from
		};
		console.log(Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.getItemsByPagination +  Ti.App.Properties.getString("selected_shop_id") +"/sub_cat_id/" + args.subCatId + "/item/all/count/"+limit+"/from/" + from);
		loader.get(loaderArgs);
	} else {
		console.log(">>>>>>>>>>>>>>>>>>>>>>> Keyword Search <<<<<<<<<<<<<<<<<<<<<<");
		var payloadJSON = {"keyword": $.txtSearch.value};
		var loaderArgs = {
			callbackFunction : callBackInfiniteScrolling,
			payload          : payloadJSON,
			url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postItemSearch +  Ti.App.Properties.getString("selected_shop_id") +"/sub_cat_id/" + args.subCatId + "/item/all/count/"+limit+"/from/" + from + '/keyword/' + $.txtSearch.value
		};
		console.log(Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postItemSearch +  Ti.App.Properties.getString("selected_shop_id") +"/sub_cat_id/" + args.subCatId + "/item/all/count/"+limit+"/from/" + from + '/keyword/' + $.txtSearch.value);
		loader.post(loaderArgs);
	}
	
	
});

var loadLanguage = function() {
	$.lblSearchKeyword.text = Alloy.CFG.Languages.lblSearchKeyword;
};

$.txtSearch.addEventListener('return', function(e) {
	doSearch();
});

var doSearch = function() {
	$.txtSearch.blur();
    console.log("Search Keyword : " + $.txtSearch.value);
	var limit = Alloy.CFG.LimitOfPosts;
	var from = 0;
	var payloadJSON = {"keyword": $.txtSearch.value};
	var loaderArgs = {
		callbackFunction : callBackDoSearch,
		payload          : payloadJSON,
		url              : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postItemSearch +  Ti.App.Properties.getString("selected_shop_id") +"/sub_cat_id/" + args.subCatId + "/item/all/count/"+limit+"/from/" + from
	};
	console.log(Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postItemSearch +  Ti.App.Properties.getString("selected_shop_id") +"/sub_cat_id/" + args.subCatId + "/item/all/count/"+limit+"/from/" + from);
	loader.post(loaderArgs);
};

var callBackDoSearch = function(respData) {
	if(respData != null) {
		console.log("<<<<< Total Search Results >>>> : " + respData.length);
		args.items = respData;
		resetGridControls();
		_psScrollView.removeAllView();
		for(var i=0; i<args.items.length; i++) {
			var scale = {
					width : args.items[i].images[0].width,
					height :  args.items[i].images[0].height
			};
			var params = {
				image     : Alloy.CFG.Urls.imagePathURL + args.items[i].images[0].path,
				title     : args.items[i].name,
				like      : i * 21,
				msg       : i * 13,
				scale     : scale,
				viewWidth : ITEM_WIDTH,
				item      : args.items[i]
			};
			
			addView(itemGridLayout(params));
			
		}
	
	} else {
		_psScrollView.removeAllView();
		dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.itemNotFoundInSearch);
	}
	
	
};

var resetGridControls = function() {
	ITEM_WIDTH = 160;
	NUM_OF_COL = 0;
	SCREEN_WIDTH = 0;
	SCREEN_HEIGHT = 0;
	EXTRA_PADDING = 0;
	ITEMS_HEIGHT = [];
	ACTUAL_TOTAL_WIDTH = 0;
	
	if(Alloy.isTablet) {
		ITEM_WIDTH = 200;
	}
	
	SCREEN_WIDTH = __.getScreenWidth();
	SCREEN_HEIGHT = __.getScreenHeight();
	
	calculateCol();
};


init();
