var args = arguments[0] || {};
var fontIconLoader = require("icomoonlib");
var psAnimation = require('animation');
var __ = require('platformSupport');
var loadingWindow = require('loadingWindow');
var dialogBox = require("psdialog");

if (__.isiOS7Plus()) {
	$.AppWrapper.top = 20;
}

var init = function() {
	loadingWindow.endLoading();
	loadIcon();
	$.lblCancelMessage.text = Alloy.CFG.Languages.lblCancelMessage;
	$.mainTitle.text = Alloy.CFG.Languages.lblPaymentCancel;
	applyAnimation();
};

var applyAnimation = function() {
	psAnimation.slowlyAppear($.paymentCancelView);
};

var closeWindow = function() {
	psAnimation.out($.paymentCancel);
};

var gotoBasket = function() {
	
	psAnimation.close($.paymentCancel);
	
	console.log("+++++ Already Close All Window From Background [Payment Cancel] +++++++++ ");
	
	var closeCheckout = Alloy.Globals.closeCheckout;
	closeCheckout();
	var closeBasket = Alloy.Globals.closeBasket;
	closeBasket();
	
	var contentView = Alloy.createController("basket").getView();
	psAnimation.in(contentView);
};

var clearBasket = function() {
	//Alloy.Collections.basket.deleteAll();
	console.log("Delete Basket items by shop id : " + args.checkout_shop_id);
	Alloy.Collections.basket.deleteRecord({
	  query : {
	     sql : "WHERE shop_id=?",
	     params : args.checkout_shop_id
	   }
	});
	
	console.log("Delete Attribute by shop id : " + args.checkout_shop_id);
	Alloy.Collections.item_attribute.deleteRecord({
	  query : {
	     sql : "WHERE shop_id=?",
	     params : args.checkout_shop_id
	   }
	}); 
	
	console.log("++++++++ After Payemnt Suucess, Refresh Basket Item Count at Item Detail ++++++++++");
	var itemCountRefresh = Alloy.Globals.itemCountRefresh;
	itemCountRefresh();
	
	console.log("++++++++ After Payemnt Suucess, Refresh Basket Item Count at Item List ++++++++++");
	var itemCountRefreshItemList = Alloy.Globals.itemCountRefreshItemList;
	itemCountRefreshItemList();
	
	console.log("++++++++ After Payemnt Suucess, Refresh Basket Item Count at Index ++++++++++");
	var itemCountRefreshIndex = Alloy.Globals.itemCountRefreshIndex;
	var params = {
		selected_shop_id : args.checkout_shop_id
	};
	itemCountRefreshIndex(params);
	
	console.log("++++++++ After clean basket, need to close Basket, Item Detail, Checkout  ++++++++++");
	var closeCheckout = Alloy.Globals.closeCheckout;
	closeCheckout();
	var closeBasket = Alloy.Globals.closeBasket;
	closeBasket();
	var closeItemDetail = Alloy.Globals.closeItemDetail;
	closeItemDetail();
	var closeItemList = Alloy.Globals.closeItemList;
	closeItemList();
	
	dialogBox.loadCustomDialog(Alloy.CFG.Languages.appName, Alloy.CFG.Languages.cleanSuccessBasket);
};

var doClose = function() {
	closeWindow();
};


var loadIcon = function() {
	var backIcon = fontIconLoader.getIcon("panacea","back",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgBack.image = backIcon;
	
	var cancelIcon = fontIconLoader.getIcon("panacea","bag",35,{color:Alloy.CFG.Colors.IconColor});
	$.imgCancel.image = cancelIcon;
};

init();
