var args = arguments[0] || {};
var psAnimation = require('animation');
var __ = require('platformSupport');
var fontIconLoader = require("icomoonlib");

if (__.isiOS7Plus()) {
	$.AppWrapper.top = 20;
}


var closeWindow = function() {
	psAnimation.out($.paymentOption);
};

var doCheckout = function() {
	
};

var loadIcon = function() {
	var backIcon = fontIconLoader.getIcon("panacea","back",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgBack.image = backIcon;
	
	var paypalIcon = fontIconLoader.getIcon("panacea","paypal",35,{color:Alloy.CFG.Colors.IconColor});
	$.paypalIcon.image = paypalIcon;
	
	var codIcon = fontIconLoader.getIcon("panacea","docs",35,{color:Alloy.CFG.Colors.IconColor});
	$.codIcon.image = codIcon;
	
	var btIcon = fontIconLoader.getIcon("panacea","creditcard",35,{color:Alloy.CFG.Colors.IconColor});
	$.btIcon.image = btIcon;
	
};

var paypalCheckout = function() {
	var params = {
		method : "paypal"
	};
	var contentView = Alloy.createController("checkout",params).getView();
	psAnimation.in(contentView);
};

var codCheckout = function() {
	var params = {
		method : "cod"
	};
	var contentView = Alloy.createController("checkout",params).getView();
	psAnimation.in(contentView);
	
};

var btCheckout = function() {
	var params = {
		method : "bank"
	};
	var contentView = Alloy.createController("checkout",params).getView();
	psAnimation.in(contentView);
};

Alloy.Globals.closePaymentOption = function() {
	console.log("Fired >>>>>>>>> closeWindows At closePaymentOption.js");
	psAnimation.close($.paymentOption);
};

var init = function() {
	loadIcon();
	
	if(Alloy.CFG.Payment_Option.paypal != "on") {
		$.paypalView.opacity = 0;
		$.paypalView.height = 0;
	}
	
	if(Alloy.CFG.Payment_Option.cod != "on") {
		$.codView.opacity = 0;
		$.codView.height = 0;
	}
	
	if(Alloy.CFG.Payment_Option.bank != "on") {
		$.bankView.opacity = 0;
		$.bankView.height = 0;
	}
	
};

var loadLanguage = function() {
	$.mainTitle.text = Alloy.CFG.Languages.lblPaymentOption;
	
	$.lblPaypal.text = Alloy.CFG.Languages.lblPaypal;
	$.lblPaypalMessage.text = Alloy.CFG.Languages.lblPaypalMessage;
	$.btnPaypalCheckout.title = Alloy.CFG.Languages.btnPaypalCheckout;
	
	$.lblCod.text = Alloy.CFG.Languages.lblCod;
	$.lblCODMessage.text = Alloy.CFG.Languages.lblCODMessage;
	$.btnCodCheckout.title = Alloy.CFG.Languages.btnCodCheckout;
	
	$.lblBt.text = Alloy.CFG.Languages.lblBt;
	$.lblBTMessage.text = Alloy.CFG.Languages.lblBTMessage;
	$.btnBtCheckout.title = Alloy.CFG.Languages.btnBtCheckout;
	
};

init();
