var __ = require("platformSupport");
var args = arguments[0] || {};
var alloyAnimation = require('alloy/animation');
var psAnimation = require('animation');
var selectedCategoryIndex = 1;
var loadingWindow = require('loadingWindow');
var loader = require("loader");
var dialogBox = require("psdialog");
var psScrollView = require('psScrollView');
var screenWidth = __.getScreenWidth();
var viewWidth = 0;


var initCategories = function(cats) {
	
	var tmpWidth = 150;
	if(Alloy.isTablet) {
		tmpWidth = 185; 
	}
	var screenWidth =  __.getScreenWidth();
	var tmpSpace = (screenWidth % tmpWidth);
	
	$.categoryView.width = screenWidth - tmpSpace;
	
	$.categoryView.contentWidth = screenWidth - tmpSpace;
	
	var screenHeight = __.getScreenHeight();
	
	if(Alloy.isTablet) {
		screenHeight = screenHeight - (154.4 + 70 + 48 + 40 +10 );
		$.categoryView.height = screenHeight;
	}
	
	var contentView = null;
	for(var i=0; i < cats.length; i++) {
		var params = {
			categoryImage: Alloy.CFG.Urls.imagePathURL + cats[i].cover_image_file,
			categoryId   : cats[i].id,
			categoryName : cats[i].name,
			subCats      : cats[i].sub_categories
		};
		contentView = Alloy.createController("categoryLayout", params).getView();
		$.categoryView.add(contentView);	
	}
};


Alloy.Globals.shakeSubCategoryTitle = function() {
	alloyAnimation.shake($.lblSubCategoryTitle, 200, null);
};

var doVisitShop = function() {
	var params = {
			name : Alloy.CFG.Languages.appName
		};										  
	var contentView = Alloy.createController("shopContact", params).getView();
	psAnimation.in(contentView);
	
};

var doFollowShop = function() {
	isFollowedChecking();
};

var isFollowedChecking = function() {
	if(Titanium.Network.online == true) {
		
		if(Ti.App.Properties.getString("userId")) {
			
			var payloadJSON = {"appuser_id":Ti.App.Properties.getString("userId")};
			
			var loaderArgs = {
				callbackFunction : callBackIsFollowedChecking,
				url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postIsFollowed + Ti.App.Properties.getString("selected_shop_id"),
				payload : payloadJSON,
			};
			loader.post(loaderArgs);
			
		} else {
				dialogBox.loadCustomDialog("Follow", Alloy.CFG.Languages.needForFollowMesssage);
		}
		
	} else {
		dialogBox.loadCustomDialog("Follow", Alloy.CFG.Languages.offlineMessage);
	}
	
};

var callBackIsFollowedChecking = function(respData) {
	if(respData != null) {
		if(respData.status == "yes") {
			dialogBox.loadCustomDialog("Follow", Alloy.CFG.Languages.followSuccessMessage);
		} else {
			followShop();
		}
	}
};


var followShop = function() {
	if(Titanium.Network.online == true) {
		
		if(Ti.App.Properties.getString("userId")) {
			
			loadingWindow.startLoading();
			
			var payloadJSON = {"appuser_id":Ti.App.Properties.getString("userId")};
			
			var loaderArgs = {
				callbackFunction : callBackFollowShop,
				url : Alloy.CFG.Urls.baseURL + Alloy.CFG.Urls.postFollowShop + Ti.App.Properties.getString("selected_shop_id"),
				payload : payloadJSON,
			};
			loader.post(loaderArgs);
		
		} else {
			dialogBox.loadCustomDialog("Follow", Alloy.CFG.Languages.needForFollowMesssage);
		}
		
	} else {
		dialogBox.loadCustomDialog("Follow", Alloy.CFG.Languages.offlineMessage);
	}
};

var callBackFollowShop = function(respData) {
	loadingWindow.endLoading();
	if(respData != null) {
		if(respData.success) {
			Ti.App.Properties.setString("selected_shop_follow_count",respData.total);
			dialogBox.loadCustomDialog("Follow", Alloy.CFG.Languages.followSuccessMessage);
		} else {
			dialogBox.loadCustomDialog("Follow", Alloy.CFG.Languages.somethingWrong);
		}
	}
};


var shopGridCalculation = function() {
	if(Alloy.isTablet) {
		$.imgShop.width = 250;
		$.imgShop.height = 176;
		
		$.shopActionView.height = 85;
		$.shopNameView.height = 85;
		
		__.setNormalFontForTablet($.lblShopName,20);
		__.setNormalFontForTablet($.lblVisitShop,20);
		__.setNormalFontForTablet($.lblFollowShop,20);
		
		__.setNormalFontForTablet($.lblItems,15);
		__.setNormalFontForTablet($.lblCategories,15);
		__.setNormalFontForTablet($.lblFollowers,15);
		
		$.lblShopDescription.height = 110;
		__.setNormalFontForTablet($.lblShopDescription,15);
		
		__.setNormalFontForTablet($.lblCategoryTitle,20);
		__.setNormalFontForTablet($.lblSubCategoryTitle,20);
		
		if(__.getScreenHeight() > 1000) {
			$.subCatContainerView.top = 60;
			$.catContainerView.top = 60;
		} else {
			$.subCatContainerView.top = 40;
			$.catContainerView.top = 40;
		}
		
	} 
	
	var columnWidth = (__.getScreenWidth() - $.imgShop.width) / 2;
	$.visitShopView.width = columnWidth - 2;
	$.followShopView.width = columnWidth - 2;
};

var loadLanguage = function() {
	$.lblCategoryTitle.text = Alloy.CFG.Languages.lblCategoriesTitle;
	$.lblSubCategoryTitle.text = Alloy.CFG.Languages.lblSubCategoriesTitle;
	$.lblVisitShop.text = Alloy.CFG.Languages.lblVisitShop;
	$.lblFollowShop.text = Alloy.CFG.Languages.lblFollowShop;
};


var applyAnimation = function() {
	psAnimation.slowlyAppear($.shopNameView);
	psAnimation.slowlyAppear($.visitShopView);
	psAnimation.slowlyAppear($.followShopView);
	psAnimation.slowlyAppear($.gotoView);
};

var init = function() {
	
	if(Ti.App.Properties.getString("shop_mode") == "multiple") {
	
		initCategories(args.shopData.categories);
		shopGridCalculation();
		
		$.lblShopName.text = args.shopData.name;
		$.lblShopDescription.text = args.shopData.description;
		$.imgShop.image = Alloy.CFG.Urls.imagePathURL + args.shopData.cover_image_file;
		
		console.log("Shop ID From API : " + args.shopData.id);
		
		Ti.App.Properties.setString("selected_shop_id", args.shopData.id);
		Ti.App.Properties.setString("selected_shop_name", args.shopData.name);
		Ti.App.Properties.setString("selected_shop_description", args.shopData.description);
		Ti.App.Properties.setString("selected_shop_phone", args.shopData.phone);
		Ti.App.Properties.setString("selected_shop_address", args.shopData.address);
		Ti.App.Properties.setString("selected_shop_image", Alloy.CFG.Urls.imagePathURL + args.shopData.cover_image_file);
		Ti.App.Properties.setString("selected_shop_image_width", args.shopData.cover_image_width);
		Ti.App.Properties.setString("selected_shop_image_height", args.shopData.cover_image_height);
		Ti.App.Properties.setString("selected_shop_image_description", args.shopData.cover_image_description);
		Ti.App.Properties.setString("selected_shop_item_count", args.shopData.item_count);
		Ti.App.Properties.setString("selected_shop_category_count", args.shopData.category_count);
		Ti.App.Properties.setString("selected_shop_follow_count", args.shopData.follow_count);
		Ti.App.Properties.setString("selected_shop_bank_account", args.shopData.bank_account);
		Ti.App.Properties.setString("selected_shop_bank_name", args.shopData.bank_name);
		Ti.App.Properties.setString("selected_shop_bank_code", args.shopData.bank_code);
		Ti.App.Properties.setString("selected_shop_branch_code", args.shopData.branch_code);
		Ti.App.Properties.setString("selected_shop_swift_code", args.shopData.swift_code);
		
		Ti.App.Properties.setString("selected_shop_paypal_email", args.shopData.paypal_email);
		Ti.App.Properties.setString("selected_shop_paypal_environment", args.shopData.paypal_environment);
		Ti.App.Properties.setString("selected_shop_paypal_appid_live", args.shopData.paypal_appid_live);
		Ti.App.Properties.setString("selected_shop_paypal_merchantname", args.shopData.paypal_merchantname);
		Ti.App.Properties.setString("selected_shop_paypal_customerid", args.shopData.paypal_customerid);
		Ti.App.Properties.setString("selected_shop_paypal_ipnurl", args.shopData.paypal_ipnurl);
		Ti.App.Properties.setString("selected_shop_paypal_memo", args.shopData.paypal_memo);
		Ti.App.Properties.setString("selected_shop_paypal_currency", args.shopData.currency_short_form);
		
		$.lblItemCount.text = args.shopData.item_count;
		$.lblCategoryCount.text = args.shopData.category_count;
		$.lblFollowerCount.text = args.shopData.follow_count;
		
		if(!OS_IOS) {
			if(Alloy.isTablet) {
				$.shopInfoView.height = 235;
				$.lblShopDescription.text = __.readMoreText(250,args.shopData.description);
				
				$.countsView.height = 65;
				$.productCountView.height = 60;
				$.categoryCountView.height = 60;
				$.followerCountView.height = 60;
				
				$.lblItems.top = 0; 
				$.lblCategories.top = 0;
				$.lblFollowers.top = 0;
				
				$.lblItemCount.top = 8;
				$.lblCategoryCount.top = 8;
				$.lblFollowerCount.top = 8;
			} else {
				$.shopInfoView.height = 120;
				$.gotoView.top = 0;
				$.lblShopDescription.text = __.readMoreText(150,args.shopData.description);
				
				$.countsView.height = 40;
				$.productCountView.height = 37;
				$.categoryCountView.height = 37;
				$.followerCountView.height = 37;
				
				__.setNormalFontForTablet($.lblItems,12);
				__.setNormalFontForTablet($.lblCategories,12);
				__.setNormalFontForTablet($.lblFollowers,12);
				
				__.setNormalFontForTablet($.lblItemCount,10);
				__.setNormalFontForTablet($.lblCategoryCount,10);
				__.setNormalFontForTablet($.lblFollowerCount,10);
				
				$.lblItems.top = 0; 
				$.lblCategories.top = 0;
				$.lblFollowers.top = 0;
				
				$.lblItemCount.top = 2;
				$.lblCategoryCount.top = 2;
				$.lblFollowerCount.top = 2;
			}
		}
	
	} else if(Ti.App.Properties.getString("shop_mode") == "single") {
		loadingWindow.endLoading();
		initCategories(args.shopData[0].categories);
		shopGridCalculation();
		
		$.lblShopName.text = args.shopData[0].name;
		$.lblShopDescription.text = args.shopData[0].description;
		$.imgShop.image = Alloy.CFG.Urls.imagePathURL + args.shopData[0].cover_image_file;
		
		console.log("Shop ID From API : " + args.shopData[0].id);
		
		Ti.App.Properties.setString("selected_shop_id", args.shopData[0].id);
		Ti.App.Properties.setString("selected_shop_name", args.shopData[0].name);
		Ti.App.Properties.setString("selected_shop_description", args.shopData[0].description);
		Ti.App.Properties.setString("selected_shop_phone", args.shopData[0].phone);
		Ti.App.Properties.setString("selected_shop_address", args.shopData[0].address);
		Ti.App.Properties.setString("selected_shop_image", Alloy.CFG.Urls.imagePathURL + args.shopData[0].cover_image_file);
		Ti.App.Properties.setString("selected_shop_image_width", args.shopData[0].cover_image_width);
		Ti.App.Properties.setString("selected_shop_image_height", args.shopData[0].cover_image_height);
		Ti.App.Properties.setString("selected_shop_image_description", args.shopData[0].cover_image_description);
		Ti.App.Properties.setString("selected_shop_item_count", args.shopData[0].item_count);
		Ti.App.Properties.setString("selected_shop_category_count", args.shopData[0].category_count);
		Ti.App.Properties.setString("selected_shop_follow_count", args.shopData[0].follow_count);
		Ti.App.Properties.setString("selected_shop_bank_account", args.shopData[0].bank_account);
		Ti.App.Properties.setString("selected_shop_bank_name", args.shopData[0].bank_name);
		Ti.App.Properties.setString("selected_shop_bank_code", args.shopData[0].bank_code);
		Ti.App.Properties.setString("selected_shop_branch_code", args.shopData[0].branch_code);
		Ti.App.Properties.setString("selected_shop_swift_code", args.shopData[0].swift_code);
		
		Ti.App.Properties.setString("selected_shop_paypal_email", args.shopData[0].paypal_email);
		Ti.App.Properties.setString("selected_shop_paypal_environment", args.shopData[0].paypal_environment);
		Ti.App.Properties.setString("selected_shop_paypal_appid_live", args.shopData[0].paypal_appid_live);
		Ti.App.Properties.setString("selected_shop_paypal_merchantname", args.shopData[0].paypal_merchantname);
		Ti.App.Properties.setString("selected_shop_paypal_customerid", args.shopData[0].paypal_customerid);
		Ti.App.Properties.setString("selected_shop_paypal_ipnurl", args.shopData[0].paypal_ipnurl);
		Ti.App.Properties.setString("selected_shop_paypal_memo", args.shopData[0].paypal_memo);
		Ti.App.Properties.setString("selected_shop_paypal_currency", args.shopData[0].currency_short_form);
		
		$.lblItemCount.text = args.shopData[0].item_count;
		$.lblCategoryCount.text = args.shopData[0].category_count;
		$.lblFollowerCount.text = args.shopData[0].follow_count;
			
		if(!OS_IOS) {
			if(Alloy.isTablet) {
				$.shopInfoView.height = 235;
				$.lblShopDescription.text = __.readMoreText(250,args.shopData[0].description);
				$.countsView.height = 65;
				$.productCountView.height = 60;
				$.categoryCountView.height = 60;
				$.followerCountView.height = 60;
				
				$.lblItems.top = 0; 
				$.lblCategories.top = 0;
				$.lblFollowers.top = 0;
				
				$.lblItemCount.top = 8;
				$.lblCategoryCount.top = 8;
				$.lblFollowerCount.top = 8;
				
			} else {
				$.shopInfoView.height = 120;
				$.gotoView.top = 0;
				$.lblShopDescription.text = __.readMoreText(150,args.shopData[0].description);
				$.countsView.height = 40;
				$.productCountView.height = 37;
				$.categoryCountView.height = 37;
				$.followerCountView.height = 37;
				
				__.setNormalFontForTablet($.lblItems,12);
				__.setNormalFontForTablet($.lblCategories,12);
				__.setNormalFontForTablet($.lblFollowers,12);
				
				__.setNormalFontForTablet($.lblItemCount,10);
				__.setNormalFontForTablet($.lblCategoryCount,10);
				__.setNormalFontForTablet($.lblFollowerCount,10);
				
				$.lblItems.top = 0; 
				$.lblCategories.top = 0;
				$.lblFollowers.top = 0;
				
				$.lblItemCount.top = 2;
				$.lblCategoryCount.top = 2;
				$.lblFollowerCount.top = 2;
			}
		}
	}
	
	loadLanguage();
	applyAnimation();
	viewWidth = ((screenWidth - 1) / 3) - 1;
	
	$.countsView.width = screenWidth - 1;
	$.productCountView.width = viewWidth;
	$.categoryCountView.width = viewWidth;
	$.followerCountView.width = viewWidth;
	
	
};

init();



