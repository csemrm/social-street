var args = arguments[0] || {};
var strings = require('strings');
var __ = require('platformSupport');
var psAnimation = require('animation');

var init = function() {
	
	$.lblSerial.text = parseInt(args.serial) + 1 + ". ";
	$.lblName.text = args.name;
	//$.lblPrice.text =  Alloy.CFG.Languages.lblPrice+args.currency_sign+ " " +args.unit_price ;
	$.lblQty.text = Alloy.CFG.Languages.lblQtyMultiply + args.qty; 
	$.lblSubtotal.text = Alloy.CFG.Languages.lblSubTotal + " :" +  " " + args.currency_sign+ args.unit_price * args.qty ;
	if(Alloy.isTablet) {
		$.dishContainer.width = 650;
	}
	
};

init();





