var args = arguments[0] || {};
var __ = require('platformSupport');
var psAnimation = require('animation');
var fontIconLoader = require("icomoonlib");
var loader = require("loader");
var validation = require("validationRules");
var dialogBox = require("psdialog");
var loadingWindow = require('loadingWindow');

if (__.isiOS7Plus()) {
	$.AppWrapper.top = 20;
}

var closeWindow = function() {
	psAnimation.out($.transactionDetail);
};

var loadIcon = function() {
	var backIcon = fontIconLoader.getIcon("panacea","back",35,{color:Alloy.CFG.Colors.IconWhite});
	$.imgBack.image = backIcon;
	
	var transIcon = fontIconLoader.getIcon("panacea","clipboard",35,{color:Alloy.CFG.Colors.MainColor});
	$.imgTrans.image = transIcon;
};

var init = function()
{
	loadIcon();
	
	$.mainTitle.text = Alloy.CFG.Languages.lblTransDetail;
	$.lblTransNo.text = Alloy.CFG.Languages.lblTransactionNo + args.id;
	$.lblTransDate.text = Alloy.CFG.Languages.lblTransactionDate  + args.added;
	$.lblTotalItem.text = Alloy.CFG.Languages.lblTotalItems + args.details.length;
	$.lblTotalAmount.text = Alloy.CFG.Languages.lblTotalAmount + args.total_amount;
	$.lblTransStatus.text = "Transaction Status : " + args.transaction_status;
	
	for(var i=0; i<args.details.length; i++)
	{
		oneItem = Alloy.createController('transactionItemLayout', args.details[i]).getView();
		$.transItemView.add(oneItem);		
	}
	
	if(!OS_IOS) {
		$.strike1.top = -30;
	}
	
	applyAnimation();
};

var applyAnimation = function() {
	psAnimation.slowlyAppear($.midContainer);
};

$.transactionDetail.addEventListener('open', function() {
	init();
	__.hideActionBar($.transactionDetail);
});

