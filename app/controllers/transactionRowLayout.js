var args = arguments[0] || {};
var strings = require('strings');
var __ = require('platformSupport');
var fontIconLoader = require("icomoonlib");
var loadingWindow = require('loadingWindow');
var psAnimation = require('animation');

var init = function(){
	
	$.lblTransNo.text = Alloy.CFG.Languages.lblTransactionNo + args.id;
	$.lblTotalItem.text = Alloy.CFG.Languages.lblTotalItems + args.details.length;
	$.lblTotalAmount.text = Alloy.CFG.Languages.lblTotalAmount + args.total_amount;
	$.lblTransStatus.text = Alloy.CFG.Languages.lblTransStatus + args.transaction_status;
	$.lblTransDate.text = args.added;
	
	if(Alloy.isTablet) {
		if(!OS_IOS) {
			
			$.imgStrike.top = -25;
		}
	}
	
	$.transContainer.width = __.getScreenWidth() - 20;
	
	var timeIcon = fontIconLoader.getIcon("panacea","clock-o",35,{color:Alloy.CFG.Colors.ItemIconColor_Dark});
	$.imgTime.image = timeIcon;
	
	var transIcon = fontIconLoader.getIcon("panacea","clipboard",35,{color:Alloy.CFG.Colors.transctionRowColor});
	$.imgTrans.image = transIcon;
	
	applyAnimation();
	
	loadingWindow.endLoading();
};

var openTransDetail = function()
{
	var contentView = Alloy.createController("transactionDetail", args).getView();
	psAnimation.in(contentView);
};

var applyAnimation = function() {
	psAnimation.slowlyAppear($.transContainer);
};

init();
