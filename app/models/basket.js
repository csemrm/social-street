exports.definition = {
	config: {
		columns: {
		    "id": "INTEGER PRIMARY KEY",
		    "item_id": "INTEGER",
		    "shop_id": "INTEGER",
		    "user_id": "INTEGER",
		    "name": "TEXT",
		    "description": "TEXT",
		    "unit_price": "REAL",
		    "discount_percent" : "REAL",
		    "qty": "INTEGER",
		    "image": "TEXT",
		    "currency_sign" : "TEXT",
		    "added": "TEXT"
		},
		adapter: {
			type: "sql",
			collection_name: "basket",
			"idAttribute": "id"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
			deleteAll : function() {
	 
			        var collection = this;
			 
			        var sql = "DELETE FROM " + collection.config.adapter.collection_name;
			        db = Ti.Database.open(collection.config.adapter.db_name);
			        db.execute(sql);
			        db.close();
			 
			        collection.trigger('sync');
		 
		     },
		     updateRecord : function(opts) {            
			       var collection = this;
			       var dbName = collection.config.adapter.db_name;
			       var table = collection.config.adapter.collection_name;
			       var columns = collection.config.columns;
			       var names = [], whereQ = [], values=[];       
			       
			       for (var i in opts.query.columns) {
			           names.push(opts.query.columns[i]+"=?");
			       }
			       for (var i in opts.query.whereKey) {
			           whereQ.push(opts.query.whereKey[i]+"=?");
			       }
			                
			        //Values of Set Columns and Where Condition
			        for (var j in opts.query.values) {
			            values.push(opts.query.values[j]);
			        }
			        for (var k in opts.query.whereValue) {
			            values.push(opts.query.whereValue[k]);
			        }
			                
	                var sql = "UPDATE " + table + " SET " + 
	                 names.join(",") + " WHERE "+ whereQ.join(" AND ");
	               
	               console.log(sql);  
	               console.log(values);  
			
			       db = Ti.Database.open(collection.config.adapter.db_name);
			       db.execute(sql, values);
			       db.close();
			       collection.trigger('sync');
            },
            deleteRecord : function(opts) {
			       var collection = this;
			       var dbName = collection.config.adapter.db_name;
			       var table = collection.config.adapter.collection_name;
			       var columns = collection.config.columns;
			       var names = [], q = [];
			       for (var k in opts.query.columns) {
			            names.push(opts.query.columns[k]);
			            q.push("?");
			       }
			       var sql = "DELETE FROM " + table + " " + opts.query.sql;
			
			       db = Ti.Database.open(collection.config.adapter.db_name);
			       db.execute(sql, opts.query.params);
			       db.close();
			       collection.trigger('sync');
            }
		});

		return Collection;
	}
};