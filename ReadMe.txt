Mokets is mobile commerce application which can setup and run your business on mobile platforms such as iOS and Android.

Used Technology:
- Titanium SDK : 3.4.0 & above (Recommended 3.4.0 or 3.4.1)
- Alloy MVC
- Ti.Map module 
- Ti.admob module
- dk.napp.social module (iOS)
- facebook module (Android)
- CodeIgniter
- Bootstrap
- JSON 

Release Note
- v(1.0.0)
- 2 Jan 2015

Release Note
- v(1.1.0)
- 28 Jan 2015

Release Note
- v(1.1.1)
- 30 Jan 2015

Release Note
- v(1.1.2)
- 31 Jan 2015

Release Note
- v(1.1.3)
- 20 Feb 2015

Release Note
- v(1.1.4)
- 12 Mar 2015


Release Note
- v(1.1.5)
- 30 Mar 2015


Release Note
- v(1.1.6)
- 8 Apr 2015

Release Note
- v(1.1.7)
- 19 May 2015