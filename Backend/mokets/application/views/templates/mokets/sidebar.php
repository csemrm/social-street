<ul class="nav nav-sidebar teamps-sidebar">
	<li class="toggle-nav">
		<a href='#'>
			</span>Menus
			<span class="glyphicon glyphicon-resize-horizontal pull-right menu-icon">  
		</a>
	</li>
	<li>
		<a href='<?php echo site_url('dashboard');?>'>
			</span>Dashboard Home
			<span class="glyphicon glyphicon-home pull-right">
		</a>
	</li>
	<?php
		foreach($allowed_modules->result() as $module){
			if($module->is_show_on_menu == 1) {
				echo "<li><a href='".site_url($module->module_name)."'>".
					$module->module_desc."<span class='".$module->module_icon." pull-right'></span></a></li>";
			}
		}
	?>
	
	<?php 
		if(!$this->session->userdata('is_shop_admin')) {
	?>
	<li><a href="<?php echo site_url('backup');?>">
			Exports<span class="glyphicon glyphicon-export pull-right"></span>
		</a>
	</li>
	<?php } ?>	
</ul>


<script>
$('.toggle-nav').click(function(e){
	e.preventDefault();
	$('.sidebar').toggleClass('teamps-sidebar');
	$('.sidebar').toggleClass('teamps-sidebar-open');
	
	var main = $('.main');
	main.toggleClass('teamps-sidebar-push');
	
	if (main.hasClass('col-md-11')) {
		main.removeClass('col-md-11');
		main.addClass('col-md-10');
	} else {
		main.removeClass('col-md-10');
		main.addClass('col-md-11');
	}
});
</script>